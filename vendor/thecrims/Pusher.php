<?php

namespace Thecrims;

use Zend\Http\Client;
use Zend\Http\Request;

class Pusher {
    private $appKey;
    private $appHost;
    
    public function __construct($appHost = 'http://localhost:8080', $appKey = 'thecrims-pusher-key')
    {
        $this->appHost = $appHost;
        $this->appKey = $appKey;
    }
    
    public function trigger($channel, $event, $data = [])
    {
        if (is_string($channel) === true) {
            $channel = [$channel];
        }
        
        $this->send($channel, $event, $data);
    }
    
    private function send($channel, $event, $data) {
        $client = new Client("{$this->appHost}/trigger?auth={$this->appKey}");
        $client->setMethod(Request::METHOD_POST);
        $client->setParameterPost([
            'channels' => $channel,
            'name' => $event,
            'data' => $data
        ]);
        
        $response = $client->send();
        return $response->isSuccess();
    }
}
