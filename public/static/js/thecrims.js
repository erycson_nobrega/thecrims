var tcApp;tcApp=tcApp||(function(){var subscriptions=['system'];return{subscribe:function(channel){subscriptions.push(channel);},subscriptions:function(){return subscriptions;}};})();$(document).keydown(function(event){if(event.keyCode==116){event.preventDefault();event.stopPropagation();if(navigator.appVersion.indexOf('MSIE')!=-1){event.keyCode=0;event.returnValue=false;}
return false;}
return true;});$(document).keyup(function(event){if(event.keyCode==116){window.location.reload();}
return true;});function redirect(url){window.location.href=url;}
function openWindow(url,width,height){leftVal=(screen.width-width)/2;
topVal=(screen.height-height)/2;
newWindow=window.open(url,'TheCrims','width='+width+',height='+height+',left='+leftVal+',top='+topVal+',toolbar=no,directories=no,status=no,menubar=no,copyhistory=no,scrollbars=yes,resizable=yes');}
function checkForm(value_array,msg){var choosed;for(var i=0;i<value_array.length;i++)
{if(value_array[i].checked)
{choosed=true;}}
if(!choosed)
{alert(msg);return false;}
else
{return true;}}
function confirmChoice(){if(confirm(langConfirm)){return true;}else{removeWait();$(':submit').show();$(':submit').removeAttr('readonly');$(':submit').removeAttr('disabled');$(':button').show();$(':button').removeAttr('readonly');$(':button').removeAttr('disabled');return false;}}
function checkBox(value){if(value=='-'){alert(langCheckbox);removeWait();$(":submit").show();return false;}
return true;}
function showhide(value,value_array,gang){for(var i=0;i<value_array.length;i++)
{var element;if(gang==true)
{obj=document.getElementById('idgang'+ value_array[i]);obj.style.display="none";}
else
{obj=document.getElementById('id'+ value_array[i]);obj.style.display="none";}}
if(value!='-')
{if(gang==true)
{obj=document.getElementById('idgang'+ value);obj.style.display="block";}
else
{obj=document.getElementById('id'+ value);obj.style.display="block";}}}
function toggleDiv(item)
{obj=document.getElementById(item);visible=(obj.style.display!="none");key=document.getElementById("x"+ item);if(visible)
{obj.style.display="none";}
else
{obj.style.display="block";}}
function toggle(item)
{obj=document.getElementById(item);visible=(obj.style.display!="none");key=document.getElementById("x"+ item);if(visible)
{obj.style.display="none";}
else
{obj.style.display="block";}}
function avatarBorder(id,what)
{choosen=document.getElementById('avatar').value;if(what=='click'){for(i=1;i<100;i++){obj=document.getElementById(i);if(obj){obj.style.border='';}}
document.getElementById(id).style.border='1px solid red';document.getElementById(id).value=id;document.getElementById('avatar').value=id;}}
function live_gametime(days,hours,minutes,dayText,multiplier,elementId){minutes+=1;if(minutes>59){minutes=0;hours+=1;}
if(hours>23){hours=0;days+=1;}
var textDays=days;var textHours=hours;var textMinutes=minutes;if(minutes<10){textMinutes="0"+minutes;}
if(hours<10){textHours="0"+hours;}
var obj_gametime=document.getElementById(elementId);obj_gametime.innerHTML=dayText+" "+textDays+" "+textHours+":"+textMinutes;setTimeout("live_gametime("+days+","+hours+","+minutes+",'"+dayText+"',"+multiplier+",'"+elementId+"')",60000/multiplier);}
function ToggleCheckAll(trigger){d=document;el=d.getElementsByTagName('INPUT');for(i=0;i<el.length;i++){if(trigger.checked==true){value=true;}
else{value=false;}
if(trigger.form==el[i].form){el[i].checked=value;}}}
function openPopup(popURL){var popLeft=(screen.width- 370)/ 2;
var popTop=(screen.height- 525)/ 2;
passPop=window.open(popURL,"","width=370,height=525,top="+popTop+",left="+popLeft+",")}
function setSingleCrimeInfo(crimeId){var crime=getSingleCrimeInfo(crimeId);if(typeof crime=='undefined'){$('#singlecrimeinfo').hide();return;}
$('#singlecrimeinfo-name').html(crime.translatedname);$('#singlecrimeinfo-energy').html(crime.energy);if(crime.spirit==0)crime.spirit='-';$('#singlecrimeinfo-spirit').html(crime.spiritname);$('#singlecrimeinfo-difficulty').html(crime.difficulty);$('#singlecrimeinfo-reward').html(crime.min_reward+" - "+crime.max_reward);if(typeof crime.successprobability!='undefined'){$('#singlecrimeinfo-successprobability').html(crime.successprobability+'%');$('#singlecrimeinfo-successprobability-image').attr('src',crime.successprobability_image);}
$('#singlecrimeinfo').show();}
function setGangCrimeInfo(crimeId){var crime=getGangCrimeInfo(crimeId);if(typeof crime=='undefined'){$('#gangcrimeinfo').hide();return;}
$('#gangcrimeinfo-name').html(crime.translatedname);$('#gangcrimeinfo-requiredmembers').html(crime.requiredmembers);$('#gangcrimeinfo-cover').html(crime.cover);$('#gangcrimeinfo-difficulty').html(crime.difficulty);$('#gangcrimeinfo-reward').html(crime.min_reward+" - "+crime.max_reward);$('#gangcrimeinfo-requiredweapons').html(crime.requiredweapons);$('#gangcrimeinfo-requiredstats').html(crime.requiredstats);$('#gangcrimeinfo-userpower').html(crime.user_power);$('#gangcrimeinfo').show();}
$(document).on('pjax:start',function(){showWait()}).on('pjax:end',function(){removeWait()})
function loadAjax(page){$.pjax({url:page,container:'div.main-content',type:'GET'});}
function submitForm(form){var url=form.attr('action');var data=form.serialize();$.post(url,form.serialize());return false;}
function renderJsonResult(json,datafunction){object=$.parseJSON(json);if(object.redirect!=undefined&&object.redirect!=''){redirect(object.redirect)
return;}
if(object.message!=undefined&&object.message!=''){$('div#statusbox').html(object.message);}
if(object.html!=undefined&&object.html!=''){$('div.main-content').html(object.html);}
if(object.user!=undefined){updateUserObject(object.user);if(object.user.friends_sound!=undefined){var sound=$('<embed autoplay="true" height="0" width="0" />');sound.attr('src',object.user.friends_sound);$('body').append(sound);}}
if(datafunction!=undefined&&datafunction!=''){object.data_function=datafunction;}
if(object.data!=undefined&&object.data!=''&&object.data_function!=undefined&&object.data_function!=''){object.data_function(object.data);}
onReady();}
function updateEvents(){$(".ajaxForm").unbind();$(".ajaxForm").submit(function(event){event.preventDefault();return submitForm($(this));});}
function showWait(){}
function removeWait(){}
function getObject(obj){var theObj;if(document.all){if(typeof obj=="string"){return document.all(obj);}else{return obj.style;}}
if(document.getElementById){if(typeof obj=="string"){return document.getElementById(obj);}else{return obj.style;}}
return null;}
function toCount(entrance,exit,text,characters){var entranceObj=getObject(entrance);var exitObj=getObject(exit);var length=characters- entranceObj.value.length;if(length<=0){length=0;text='<span class="disable"> '+text+' </span>';entranceObj.value=entranceObj.value.substr(0,characters);}
exitObj.innerHTML=text.replace("{CHAR}",length);}
function renderUserList(listId,users){if(typeof users==='undefined'||users.length<=0){return false;}
for(var userId in users){addUserToList(listId,users[userId]);}}
function addUserToList(listId,user){var appendString='';if(typeof userListAppendFunctionName!="undefined"){appendString=window[userListAppendFunctionName](user);}
var html='<li><div class="user_list_avatar">'+user.avatar+'<div class="user_list_flag">'+user.flag+'</div></div><div><div class="user_list_username">'+user.username_link+'</div><div class="user_list_profession">'+user.profession+'</div><div class="user_list_respect">'+user.respect_display+'</div>';html=html+'</div><div class="user_list_protection">'+user.under_protection_text+'</div>'+appendString+'</li>';$(listId).append(html);}
function populateNightlifeVisitorList(users){$('#visitors').empty();renderUserList('#visitors',users);}
function updateNightlifeVisitors(){$.ajaxSetup({cache:false});$.ajax({url:window.SELF+"?action=getVisitors",success:function(data){if(typeof data==='undefined'||data.length<=0){redirect(window.location.href);}
populateNightlifeVisitorList(data);},dataType:"json",timeout:30000});}
function updateUI(){$('.jqui-progressbar').each(function(){var value=parseInt($(this).data('value'));progressbar=$(this).empty().progressbar({value:value});if(value>=100){progressbar.addClass("end");}});}
function updateGangRobbery(){$.ajaxSetup({cache:false});$.ajax({url:window.SELF+"?action=getGangRobbery",success:function(result){if(result.robbery!==undefined&&result.robbery.perform=='N'){populateGangRobberyParticipantLists(result.invitations,result.robbery);}else{location.reload();}},dataType:"json",timeout:30000});}
function updateGangRobberyStatus(){$.ajax({url:window.SELF+"?action=getGangRobberyStatus",success:function(status){if(status==false){location.reload();}},dataType:"json",timeout:30000});}
function populateGangRobberyParticipantLists(users,robbery){$('#gang_robbery_accepted').empty();$('#gang_robbery_waiting').empty();$('#gang_robbery_declined').empty();var accepts=0;var currentUserId=$.cookie('current_user_id');for(var userId in users){user=users[userId];if(user.status==1){addUserToList('#gang_robbery_accepted',user);accepts++;}
else if(user.status==2){addUserToList('#gang_robbery_declined',user);}
else{addUserToList('#gang_robbery_waiting',user);}
if(user.id==currentUserId){selfStatus=user.status;}}
var participantsPercentage=accepts/requiredParticipants*100;$('#gang_robbery_participants_progress').css('width',participantsPercentage+'%');$('#gang_robbery_participants_accepted').html(accepts);if(selfStatus==1){$('#gang_robbery_controls_abort').show();}
if(participantsPercentage>=100&&selfStatus==1){$('#gang_robbery_controls_submit').show();}else{$('#gang_robbery_controls_submit').hide();}
if(users.length<=0||robbery.required_members>users.length){$('#gang_robbery_controls_submit').hide();$('#gang_robbery_controls_abort').show();}}
function addParticipantToList(list,user){$(list).append('<li>'+user.username+'</li>');}
function onUpdateProfile(data){for(var key in data){switch(key){case'user-friends-icon':case'user-gangcenter-icon':$('#'+key).attr('src',data[key]);break;}}}
function onMessage(data){$.gritter.add({title:data.title,sticky:data.sticky,text:data.text,image:data.image});}
function pushListener(data){data=$.parseJSON(data);if(typeof data.command!=='undefined'){switch(data.command){case'chatmessage':$("#chat").append(data.data);$('#chat').scrollTop($('#chat').height());break;case'gritter':$.gritter.add({title:data.title,sticky:data.sticky,text:data.text,image:data.image});break;case'updatedrugdealerstock':updateDrugStockQuantities();break;case'updatenightlifevisitors':updateNightlifeVisitors();break;case'updateprofile':for(var key in data){switch(key){case'user-messages-icon':$('#'+key).attr('src',data[key]);$.ajax({url:"/msgcenter.php?page=getnewmessages",success:function(messages){messages=$.parseJSON(messages);for(i in messages){$.gritter.add({title:messages[i].title,sticky:false,text:messages[i].text,image:messages[i].image});}},timeout:30000});break;case'user-friends-icon':$('#'+key).attr('src',data[key]);break;case'user-gangcenter-icon':$('#'+key).attr('src',data[key]);break;}}
break;}}}