/**
 * Implementação simples do sistema Pusher para o clone do TheCrims
 *
 * @author ExtremsX
 * @version 1.1
 * @returns {Pusher}
 */

function Pusher() {
    this.host = '127.0.0.1';
    this.port = 4567;
    this.socket = io('http://' + this.host + ':' + this.port);
    this.channels = {};
    var self = this;
    
    this.socket.on('data', function(data) {
        if (data.channel) {
            self.channels[data.channel].onEvent(data);
        } else {
            console.error('Canal %s não existe', data.channel);
        }
    });
}

Pusher.prototype.subscribe = function(channel) {
    this.socket.emit('pusher:subscribe', channel);
    return this.channels[channel] = new Channel(channel);
};

/**
 * Canal que fará o tratamento dos eventos recebidos
 * 
 * @param {string} name
 * @returns {Channel}
 */
function Channel(name) {
    this.name = name;
    this.events = {};
}

Channel.prototype.bind = function(eventName, callback) {
    this.events[eventName] = callback;
    console.log('Evento %s registrado', eventName);
};

Channel.prototype.onEvent = function(data) {
    if (this.events[data.event]) {
        this.events[data.event](data.data);
    } else {
        console.error('Evento %s não existe', data.event);
    }
};