$.fn.mailgun_validator=function(options){return this.each(function(){$(this).focusout(function(){run_validator($(this).val(),options);});});};function run_validator(address_text,options){if(!address_text){return;}
if(address_text.length>512){error_message='Stream exceeds maxiumum allowable length of 512.';if(options&&options.error){options.error(error_message);}
else{console.log(error_message);}
return;}
if(options&&options.in_progress){options.in_progress();}
if(options&&options.api_key==undefined){console.log('Please pass in api_key to mailgun_validator.')}
var success=false;$.ajax({type:"GET",url:'https://api.mailgun.net/v2/address/validate?callback=?',data:{address:address_text,api_key:options.api_key},dataType:"jsonp",crossDomain:true,success:function(data,status_text){success=true;if(options&&options.success){options.success(data);}},error:function(request,status_text,error){success=true;error_message='Error occurred, unable to validate address.';if(options&&options.error){options.error(error_message);}
else{console.log(error_message);}}});setTimeout(function(){error_message='Error occurred, unable to validate address.';if(!success){if(options&&options.error){options.error(error_message);}
else{console.log(error_message);}}},30000);}