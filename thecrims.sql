/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50615
Source Host           : 127.0.0.1:3306
Source Database       : thecrims

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2015-11-19 11:14:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for arm
-- ----------------------------
DROP TABLE IF EXISTS `arm`;
CREATE TABLE `arm` (
  `arm_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `arm_section` enum('armors','weapons') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'weapons',
  `arm_type` enum('percentage','sum') COLLATE utf8_unicode_ci NOT NULL,
  `arm_strength` smallint(5) unsigned NOT NULL,
  `arm_tolerance` smallint(5) unsigned NOT NULL DEFAULT '0',
  `arm_cash` int(10) unsigned NOT NULL,
  `arm_credits` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `arm_image` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `arm_required_level` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'Nível necessario',
  PRIMARY KEY (`arm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of arm
-- ----------------------------
INSERT INTO `arm` VALUES ('1', 'weapons', 'sum', '5', '0', '132', '0', 'monkeybat.jpg', '0');
INSERT INTO `arm` VALUES ('2', 'weapons', 'sum', '10', '0', '550', '0', 'knife.jpg', '0');
INSERT INTO `arm` VALUES ('3', 'weapons', 'sum', '15', '0', '990', '0', 'sword2.jpg', '0');
INSERT INTO `arm` VALUES ('4', 'weapons', 'sum', '25', '0', '1320', '0', 'chainsaw.jpg', '0');
INSERT INTO `arm` VALUES ('5', 'weapons', 'sum', '30', '0', '3080', '0', 'glock.jpg', '0');
INSERT INTO `arm` VALUES ('6', 'weapons', 'sum', '40', '0', '7810', '0', 'benellisuper90.jpg', '0');
INSERT INTO `arm` VALUES ('7', 'weapons', 'sum', '45', '0', '9570', '0', 'mp5.jpg', '0');
INSERT INTO `arm` VALUES ('8', 'weapons', 'sum', '55', '0', '17050', '0', 'uzi.jpg', '0');
INSERT INTO `arm` VALUES ('9', 'weapons', 'sum', '60', '0', '22550', '0', 'ak47.jpg', '0');
INSERT INTO `arm` VALUES ('10', 'weapons', 'sum', '70', '0', '33000', '0', 'coltm4a1.jpg', '0');
INSERT INTO `arm` VALUES ('11', 'weapons', 'sum', '75', '0', '38500', '0', 'deagle.jpg', '0');
INSERT INTO `arm` VALUES ('12', 'weapons', 'sum', '110', '0', '82500', '0', 'sniper.jpg', '0');
INSERT INTO `arm` VALUES ('13', 'weapons', 'percentage', '1', '0', '6532452', '0', 'raygun.jpg', '0');
INSERT INTO `arm` VALUES ('14', 'weapons', 'percentage', '2', '0', '112999484', '0', 'machine_gun.jpg', '0');
INSERT INTO `arm` VALUES ('15', 'weapons', 'percentage', '4', '0', '401789209', '0', 'bazooka.jpg', '0');
INSERT INTO `arm` VALUES ('16', 'weapons', 'percentage', '7', '0', '557621826', '0', 'gail.jpg', '0');
INSERT INTO `arm` VALUES ('17', 'weapons', 'percentage', '8', '0', '665629452', '0', 'bfg.jpg', '0');
INSERT INTO `arm` VALUES ('18', 'weapons', 'percentage', '10', '0', '1008020703', '0', 'extreme.jpg', '0');
INSERT INTO `arm` VALUES ('19', 'weapons', 'percentage', '10', '0', '0', '30', 'extreme.jpg', '0');
INSERT INTO `arm` VALUES ('20', 'armors', 'sum', '0', '6', '165', '0', 'diper.jpg', '0');
INSERT INTO `arm` VALUES ('21', 'armors', 'sum', '0', '25', '8800', '0', 'shining_body_armor.jpg', '0');
INSERT INTO `arm` VALUES ('22', 'armors', 'sum', '0', '55', '49500', '0', 'body_armour.jpg', '0');
INSERT INTO `arm` VALUES ('23', 'armors', 'sum', '0', '150', '440000', '0', 'nano_fiber_combat_jacket.jpg', '0');
INSERT INTO `arm` VALUES ('24', 'armors', 'sum', '0', '2000', '11000000', '0', 'nomex_plated_armour.jpg', '5');

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business` (
  `bus_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `bus_type` enum('whorehouse','nightlife') COLLATE utf8_unicode_ci NOT NULL,
  `bus_capacity` smallint(4) unsigned NOT NULL,
  `bus_price` int(10) unsigned NOT NULL,
  `bus_image` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of business
-- ----------------------------
INSERT INTO `business` VALUES ('1', 'nightlife', '25', '25000', 'nightclub.jpg');
INSERT INTO `business` VALUES ('2', 'whorehouse', '10', '100000', 'whorehouse.jpg');
INSERT INTO `business` VALUES ('3', 'nightlife', '100', '250000', 'rave.jpg');
INSERT INTO `business` VALUES ('4', 'whorehouse', '25', '250000', 'hooker_manison.jpg');

-- ----------------------------
-- Table structure for drug
-- ----------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug` (
  `drug_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `drug_price_buy` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `drug_price_sell` decimal(10,2) unsigned NOT NULL,
  `drug_stock` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`drug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of drug
-- ----------------------------
INSERT INTO `drug` VALUES ('1', '5.52', '6.00', '820326241');
INSERT INTO `drug` VALUES ('2', '6.67', '7.00', '298171250');
INSERT INTO `drug` VALUES ('3', '9.49', '9.00', '1293602010');
INSERT INTO `drug` VALUES ('4', '9.30', '9.00', '146398151');
INSERT INTO `drug` VALUES ('5', '14.00', '14.00', '7321554');
INSERT INTO `drug` VALUES ('6', '29.82', '30.00', '712238');
INSERT INTO `drug` VALUES ('7', '96.64', '97.00', '71861');
INSERT INTO `drug` VALUES ('8', '58.36', '58.00', '11404');
INSERT INTO `drug` VALUES ('9', '73.51', '74.00', '645649');
INSERT INTO `drug` VALUES ('10', '297.84', '298.00', '3000537');
INSERT INTO `drug` VALUES ('11', '12.00', '12.00', '1750');
INSERT INTO `drug` VALUES ('12', '161.65', '162.00', '109681');
INSERT INTO `drug` VALUES ('13', '151.04', '151.00', '160570');
INSERT INTO `drug` VALUES ('14', '37.85', '38.00', '336453');

-- ----------------------------
-- Table structure for hooker
-- ----------------------------
DROP TABLE IF EXISTS `hooker`;
CREATE TABLE `hooker` (
  `hok_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `hok_payout` int(10) unsigned NOT NULL,
  `hok_price` int(10) unsigned NOT NULL,
  `hok_price_sum` int(10) unsigned NOT NULL,
  `hok_for_sell` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hok_image` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`hok_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hooker
-- ----------------------------
INSERT INTO `hooker` VALUES ('1', '28', '400', '100', '1', 'dolly.jpg');
INSERT INTO `hooker` VALUES ('2', '47', '600', '150', '1', 'heinrich.jpg');
INSERT INTO `hooker` VALUES ('3', '56', '800', '200', '1', 'hooker2.jpg');
INSERT INTO `hooker` VALUES ('4', '84', '1200', '0', '1', 'hooker4.jpg');
INSERT INTO `hooker` VALUES ('5', '253', '3500', '0', '1', 'hooker3.jpg');
INSERT INTO `hooker` VALUES ('6', '309', '4000', '0', '1', 'bell.jpg');
INSERT INTO `hooker` VALUES ('7', '421', '5200', '0', '1', 'crystal.jpg');
INSERT INTO `hooker` VALUES ('8', '561', '7000', '0', '1', 'valerie.jpg');
INSERT INTO `hooker` VALUES ('9', '674', '8400', '0', '1', 'chessy.jpg');
INSERT INTO `hooker` VALUES ('10', '982', '12500', '0', '1', 'head_nurse.jpg');
INSERT INTO `hooker` VALUES ('11', '1263', '15000', '0', '1', 'gothic_goddess.jpg');
INSERT INTO `hooker` VALUES ('12', '1965', '24500', '0', '1', 'miss_fbi.jpg');
INSERT INTO `hooker` VALUES ('13', '2807', '30000', '0', '1', 'darling_devil.jpg');
INSERT INTO `hooker` VALUES ('14', '4210', '55000', '0', '1', 'sergeant_sexy.jpg');
INSERT INTO `hooker` VALUES ('15', '5052', '63000', '0', '1', 'jessica.jpg');
INSERT INTO `hooker` VALUES ('16', '7017', '80000', '0', '1', 'bunnie.jpg');
INSERT INTO `hooker` VALUES ('17', '8421', '100000', '0', '1', 'mrs_robinson.jpg');
INSERT INTO `hooker` VALUES ('18', '11228', '150000', '0', '1', 'mr_love.jpg');
INSERT INTO `hooker` VALUES ('19', '19648', '240000', '0', '1', 'lill_jill.jpg');
INSERT INTO `hooker` VALUES ('20', '28069', '430000', '0', '1', 'the_twins.jpg');
INSERT INTO `hooker` VALUES ('21', '39297', '650000', '0', '1', 'slimsusy.jpg');
INSERT INTO `hooker` VALUES ('22', '50525', '800000', '0', '1', 'smbabe.jpg');
INSERT INTO `hooker` VALUES ('23', '63666', '1200000', '0', '1', 'missblonde.jpg');
INSERT INTO `hooker` VALUES ('24', '527', '2200', '0', '0', 'eve.jpg');
INSERT INTO `hooker` VALUES ('25', '614', '2500', '0', '0', 'sheila.jpg');
INSERT INTO `hooker` VALUES ('26', '702', '2800', '0', '0', 'lilly.jpg');
INSERT INTO `hooker` VALUES ('27', '878', '3500', '0', '0', 'inga.jpg');
INSERT INTO `hooker` VALUES ('28', '965', '3900', '0', '0', 'louise.jpg');
INSERT INTO `hooker` VALUES ('29', '1087', '4500', '0', '0', 'woman_of_wonder.jpg');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `msg_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `msg_type` enum('hospital','square') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'square',
  `msg_day` tinyint(3) unsigned NOT NULL,
  `msg_hour` time NOT NULL,
  `msg_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '1', 'square', '1', '02:49:00', '1x1 same');
INSERT INTO `message` VALUES ('2', '1', 'square', '1', '15:44:04', 'esdsaa');
INSERT INTO `message` VALUES ('3', '1', 'square', '1', '15:44:54', 'asdasd');
INSERT INTO `message` VALUES ('4', '1', 'square', '1', '15:45:15', 'asdasd');
INSERT INTO `message` VALUES ('5', '1', 'square', '1', '15:50:58', 'dddd');
INSERT INTO `message` VALUES ('6', '1', 'square', '1', '15:51:24', 'sssssssssss');
INSERT INTO `message` VALUES ('7', '2', 'square', '1', '16:06:07', 'teste');
INSERT INTO `message` VALUES ('8', '1', 'square', '1', '16:06:14', 'teste');
INSERT INTO `message` VALUES ('9', '2', 'square', '1', '16:06:36', 'teste');

-- ----------------------------
-- Table structure for robbery
-- ----------------------------
DROP TABLE IF EXISTS `robbery`;
CREATE TABLE `robbery` (
  `rob_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `rob_difficult` int(11) unsigned NOT NULL,
  `rob_energy` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rob_spirit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rob_type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `rob_min_reward` int(11) unsigned NOT NULL DEFAULT '0',
  `rob_max_reward` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rob_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of robbery
-- ----------------------------
INSERT INTO `robbery` VALUES ('1', '3', '5', '0', '1', '9', '13');
INSERT INTO `robbery` VALUES ('2', '10', '10', '0', '1', '60', '81');
INSERT INTO `robbery` VALUES ('3', '20', '10', '0', '1', '120', '161');
INSERT INTO `robbery` VALUES ('4', '50', '10', '0', '1', '359', '482');
INSERT INTO `robbery` VALUES ('5', '100', '10', '0', '1', '855', '1148');
INSERT INTO `robbery` VALUES ('6', '200', '10', '0', '1', '2051', '2754');
INSERT INTO `robbery` VALUES ('7', '400', '10', '0', '1', '4556', '6120');
INSERT INTO `robbery` VALUES ('8', '800', '15', '0', '1', '10251', '13770');
INSERT INTO `robbery` VALUES ('9', '1500', '15', '0', '1', '19221', '25819');
INSERT INTO `robbery` VALUES ('10', '3000', '15', '0', '1', '40364', '54220');
INSERT INTO `robbery` VALUES ('11', '4000', '15', '0', '1', '53818', '72293');
INSERT INTO `robbery` VALUES ('12', '6000', '25', '0', '1', '0', '0');
INSERT INTO `robbery` VALUES ('13', '8000', '25', '0', '1', '0', '0');
INSERT INTO `robbery` VALUES ('14', '10000', '25', '0', '1', '0', '0');
INSERT INTO `robbery` VALUES ('15', '15000', '25', '0', '1', '0', '0');
INSERT INTO `robbery` VALUES ('16', '22', '25', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_accepted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_country` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_date_register` datetime NOT NULL,
  `user_date_last_login` datetime NOT NULL,
  `user_date_of_birth` date NOT NULL DEFAULT '0000-00-00',
  `user_profile_visitores` int(10) unsigned NOT NULL DEFAULT '0',
  `user_time_online` int(10) unsigned NOT NULL DEFAULT '0',
  `user_times_banned` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_profession` enum('biz','pimp','hitman','gangster') COLLATE utf8_unicode_ci NOT NULL,
  `user_stamina` tinyint(3) unsigned NOT NULL DEFAULT '100',
  `user_addiction` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_respect` int(10) unsigned NOT NULL DEFAULT '5',
  `user_genere` enum('f','m') COLLATE utf8_unicode_ci NOT NULL,
  `user_kills` int(10) unsigned NOT NULL DEFAULT '0',
  `user_deads` int(10) unsigned NOT NULL DEFAULT '0',
  `user_level` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `user_spirit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_intelligence` int(10) unsigned NOT NULL DEFAULT '5',
  `user_strength` int(10) unsigned NOT NULL DEFAULT '5',
  `user_charisma` int(10) unsigned NOT NULL DEFAULT '5',
  `user_tolerance` int(10) unsigned NOT NULL DEFAULT '5',
  `user_cash` bigint(20) unsigned NOT NULL DEFAULT '400',
  `user_credits` int(10) unsigned NOT NULL DEFAULT '0',
  `user_points_inventory` int(10) unsigned NOT NULL DEFAULT '0',
  `user_points_gang_donation` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uniq_user_email` (`user_email`) USING HASH,
  UNIQUE KEY `uniq_user_username` (`user_username`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin', 'admin@thecrims.local', '0', 'BR', '2015-03-07 07:25:05', '2015-03-07 07:25:11', '0000-00-00', '0', '0', '0', '1', 'biz', '100', '0', '1050', 'm', '0', '0', '1', '0', '1268', '1260', '838', '833', '12198', '0', '0', '0');
INSERT INTO `user` VALUES ('2', 'user1', 'user1', 'user@thecrims.local', '0', 'BR', '2015-03-20 12:15:54', '2015-03-20 12:15:57', '0000-00-00', '0', '0', '0', '1', 'biz', '100', '0', '5', 'm', '0', '0', '1', '0', '5', '5', '5', '5', '388', '0', '0', '0');
INSERT INTO `user` VALUES ('3', '', '', '', '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0', '0', '0', '1', 'biz', '100', '0', '5', 'f', '0', '0', '1', '', '5', '5', '5', '5', '400', '0', '0', '0');

-- ----------------------------
-- Table structure for user_arm
-- ----------------------------
DROP TABLE IF EXISTS `user_arm`;
CREATE TABLE `user_arm` (
  `uarm_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `arm_id` tinyint(2) unsigned NOT NULL,
  `uarm_equipped` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uarm_id`),
  KEY `fk_teste` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_arm
-- ----------------------------
INSERT INTO `user_arm` VALUES ('18', '1', '1', '0');
INSERT INTO `user_arm` VALUES ('19', '1', '1', '0');

-- ----------------------------
-- Table structure for user_business
-- ----------------------------
DROP TABLE IF EXISTS `user_business`;
CREATE TABLE `user_business` (
  `ubus_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `bus_id` tinyint(2) NOT NULL,
  `ubus_amount` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`ubus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_business
-- ----------------------------

-- ----------------------------
-- Table structure for user_drug
-- ----------------------------
DROP TABLE IF EXISTS `user_drug`;
CREATE TABLE `user_drug` (
  `udrug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `drug_id` tinyint(2) NOT NULL,
  `udrug_stock` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`udrug_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_drug
-- ----------------------------
INSERT INTO `user_drug` VALUES ('1', '1', '1', '9');
INSERT INTO `user_drug` VALUES ('2', '1', '10', '0');
INSERT INTO `user_drug` VALUES ('3', '2', '1', '2');

-- ----------------------------
-- Table structure for user_hooker
-- ----------------------------
DROP TABLE IF EXISTS `user_hooker`;
CREATE TABLE `user_hooker` (
  `uhok_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `hok_id` tinyint(2) unsigned NOT NULL,
  `uhok_amount` smallint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`uhok_id`),
  UNIQUE KEY `uniq_userhooker_owner` (`user_id`,`hok_id`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_hooker
-- ----------------------------
INSERT INTO `user_hooker` VALUES ('7', '1', '1', '1');

-- ----------------------------
-- Table structure for user_stats_gang
-- ----------------------------
DROP TABLE IF EXISTS `user_stats_gang`;
CREATE TABLE `user_stats_gang` (
  `usgang_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gang_id` int(11) NOT NULL,
  `usgang_participation` float(3,2) NOT NULL,
  PRIMARY KEY (`usgang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Histórico de participações em gangs do usuário';

-- ----------------------------
-- Records of user_stats_gang
-- ----------------------------

-- ----------------------------
-- Table structure for user_stats_profile
-- ----------------------------
DROP TABLE IF EXISTS `user_stats_profile`;
CREATE TABLE `user_stats_profile` (
  `usprof_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `usprof_day` tinyint(2) unsigned NOT NULL,
  `usprof_kills` int(11) NOT NULL,
  `usprof_respect` int(11) NOT NULL,
  PRIMARY KEY (`usprof_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Histórico do assasinatos e respeito do usuário de cada dia';

-- ----------------------------
-- Records of user_stats_profile
-- ----------------------------
DROP TRIGGER IF EXISTS `trg_update_respect`;
DELIMITER ;;
CREATE TRIGGER `trg_update_respect` BEFORE UPDATE ON `user` FOR EACH ROW BEGIN
    SET NEW.user_respect =FLOOR((NEW.user_cash / 10000) + ((NEW.user_intelligence + NEW.user_strength + NEW.user_charisma + NEW.user_tolerance) / 4));
END
;;
DELIMITER ;
