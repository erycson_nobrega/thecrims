<?php

return [
    // Hookers
    'hookers.name.1' => 'Dolly',
    'hookers.name.2' => 'Heinrich',
    'hookers.name.3' => 'Britney',
    'hookers.name.4' => 'Mount Tse Tung',
    'hookers.name.5' => 'Marilyn',
    'hookers.name.6' => 'Bell',
    'hookers.name.7' => 'Crystal',
    'hookers.name.8' => 'Valerie',
    'hookers.name.9' => 'Chessy',
    'hookers.name.10' => 'Head Nurse',
    'hookers.name.11' => 'Gothic Goddess',
    'hookers.name.12' => 'Miss FBI',
    'hookers.name.13' => 'Darling Devil',
    'hookers.name.14' => 'Sergeant Sexy',
    'hookers.name.15' => 'Jessica',
    'hookers.name.16' => 'Bunnie',
    'hookers.name.17' => 'Mrs. Robinson',
    'hookers.name.18' => 'Mr Love',
    'hookers.name.19' => 'Lill & Jill',
    'hookers.name.20' => 'The Twins',
    'hookers.name.21' => 'Slim Susy',
    'hookers.name.22' => 'SM Babe',
    'hookers.name.23' => 'Miss Blonde',
    'hookers.name.24' => 'Eve',
    'hookers.name.25' => 'Sheila',
    'hookers.name.26' => 'Lilly',
    'hookers.name.27' => 'Inga',
    'hookers.name.28' => 'Louise',
    'hookers.name.29' => 'Woman of Wonder',
    
    // Arms    
    'arms.title.weapon' => 'Weapons',
    'arms.title.armor' => 'Armors',
    'arms.tab.weapon' => 'Weapons',
    'arms.tab.armor' => 'Armors',
    'arms.text' => 'HEY STRANGER! Need some firepowah!? Or maybe an armor? You\'re going to need it man!',
    'arms.user.weapons' => 'Your weapons',
    'arms.user.strength' => 'Strength',
    'arms.user.price' => 'Price',
    'arms.user.sell' => 'Sell',
    'arms.user.manage' => 'Manage your weapons',
    'arms.user.manage.armor' => 'Manage your armors',
    'arms.market' => 'Market weapons',
    'arms.strength' => 'Strength',
    'arms.price' => 'Price',
    'arms.quantity' => 'Quantity',
    'arms.buy' => 'Buy',
    'arms.armor.text' => 'Do you value your life? Then get a bodyarmor! With one of these babies on you don\'t have to dance bullets anymore.',
    'arms.armor.market' => 'Armors',
    'arms.armor.tolerance' => 'Tolerance',
    'arms.required.level' => 'You must have completed level %d',
    
    // Arms - Weapons
    'arms.name.1'  => 'Baseball bat',
    'arms.name.2'  => 'Knife',
    'arms.name.3'  => 'Sword',
    'arms.name.4'  => 'Chainsaw',
    'arms.name.5'  => 'Glock',
    'arms.name.6'  => 'Shotgun',
    'arms.name.7'  => 'MP5',
    'arms.name.8'  => 'Uzi',
    'arms.name.9'  => 'AK 47',
    'arms.name.10' => 'Colt M4A1',
    'arms.name.11' => 'Deagle',
    'arms.name.12' => 'Sniper rifle',
    'arms.name.13' => 'Raygun',
    'arms.name.14' => 'Machine Gun',
    'arms.name.15' => 'Bazooka',
    'arms.name.16' => 'Galil',
    'arms.name.17' => 'BFG 9000',
    'arms.name.18' => 'Mean Machine',
    'arms.name.19' => 'Mean Machine',
    
    // Arms - Armors
    'arms.name.20' => 'Diaper',
    'arms.name.21' => 'Leather Jacket',
    'arms.name.22' => 'Shining body armor',
    'arms.name.23' => 'Body armor',
    'arms.name.24' => 'Nano Fiber Combat Jacket',
    'arms.name.25' => 'Nomex plated armor',
    
    // Drugs
    'drugs.name.1'  => 'Painkillers',
    'drugs.name.2'  => 'Booze',
    'drugs.name.3'  => 'Weed',
    'drugs.name.4'  => 'Magic mushrooms',
    'drugs.name.5'  => 'LSD',
    'drugs.name.6'  => 'Ecstacy',
    'drugs.name.7'  => 'Cocaine',
    'drugs.name.8'  => 'Opium',
    'drugs.name.9'  => 'Amphetamine',
    'drugs.name.10' => 'Heroin',
    'drugs.name.11' => 'Hash',
    'drugs.name.12' => 'Special K',
    'drugs.name.13' => 'Morphine',
    'drugs.name.14' => 'GHB',
    'drugs.noqtde' => 'Você deve selecionar a quantidade de drogas.', //Falta traduzir 
    'drugs.nostock' => 'The drug dealer does not have enough of this drug in stock',
    'drugs.buy' => 'You have spent $%d on %d %s',
    'drugs.notickets' => 'Você não pode mais fazer negócios com o vendedor hoje.', // Falta traduzir
    'drugs.title' => 'Drug dealer',
    'drugs.text' => 'Yo rastamon! I got some fresh stuff directly from nature! 5 stamina points for every buy/sell. 
    By the way, it will cost you 5%% to sell your stash.<br><br>
    Hints: Different events can effect the prices. Your character type will affect the price when selling drugs to the dealer.<br><br>
    I can do business with you more %d time(s) today.',
    'drugs.table.market' => 'Market drugs',
    'drugs.table.price' => 'Price/unit',
    'drugs.table.stock' => 'Stock',
    'drugs.table.buy' => 'Buy',
    'drugs.table.stock.user' => 'Your drugs',
    'drugs.table.price.user' => 'Price/unit',
    'drugs.table.stock.user' => 'Units',
    'drugs.table.sell.user' => 'Sell',
    
    //Robbery
    'robbery.title' => 'Robbery',
    'robbery.text' => 'Rumor says that the perfect crime is to stab someone with an icicle. Self-destructing evidence...<br>
Make it fast and clean then maybe you will get away with some fresh dineros. Some crimes are too hard to handle by your own, 
to succeed you will need help from your homies in the gang.<br><br>
You will discover more and harder robberies after completing new levels!<br>
Hints: The plantime of a gang crime effects the chances to succeed.',
    'robbery.name.1' => 'Shoplift',
    'robbery.name.2' => 'Old lady',
    'robbery.name.3' => 'Car break-in',
    'robbery.name.4' => 'Taxi',
    'robbery.name.5' => 'House',
    'robbery.name.6' => 'Gas station',
    'robbery.name.7' => 'Grocery store',
    'robbery.name.8' => 'Kidnapping',
    'robbery.name.9' => 'Jewellery',
    'robbery.name.10' => 'Car Saloon',
    'robbery.name.11' => 'Local bastards',
    'robbery.name.12' => 'Safety deposit',
    'robbery.name.13' => 'Rave party',
    'robbery.name.14' => 'Computer store',
    'robbery.name.15' => 'Local dealer',
    'robbery.name.16' => 'Maffia Boss',
    'robbery.name.17' => 'National museum',
    'robbery.name.18' => 'Casino',
    'robbery.name.19' => 'Russian drug king',
    'robbery.name.20' => 'Motorcycle gang',
    
    
    
    // Geral    
    'geral.nocash' => 'Not enough cash.',
    'geral.nostamina' => 'Você não tem stamina suficiente.', //Falta traduzir
    
    
];