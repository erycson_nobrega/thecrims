<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Stdlib\ArrayUtils;

use Frontend\Model\User;
use Frontend\Model\UserTable;
use Frontend\Model\Hooker;
use Frontend\Model\HookerTable;
use Frontend\Model\UserHooker;
use Frontend\Model\UserHookerTable;
use Frontend\Model\Business;
use Frontend\Model\BusinessTable;
use Frontend\Model\UserBusiness;
use Frontend\Model\UserBusinessTable;
use Frontend\Model\Arm;
use Frontend\Model\ArmTable;
use Frontend\Model\UserArm;
use Frontend\Model\UserArmTable;
use Frontend\Model\Drug;
use Frontend\Model\DrugTable;
use Frontend\Model\UserDrug;
use Frontend\Model\UserDrugTable;
use Frontend\Model\Message;
use Frontend\Model\MessageTable;
use Frontend\Model\Robbery;
use Frontend\Model\RobberyTable;

use Thecrims\Pusher;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
    
    public function getConfig() {
        $config = array();
        $configFiles = array(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/thecrims.config.php',
        );
        
        foreach ($configFiles as $file) {
            $config = ArrayUtils::merge($config, $file);
        }
        
        return $config;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Thecrims'   => __DIR__ . '/../../vendor/thecrims'
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Frontend\Model\UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    return new UserTable($tableGateway);
                },
                'UserTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                        
                'Frontend\Model\HookerTable' => function($sm) {
                    $tableGateway = $sm->get('HookerTableGateway');
                    return new HookerTable($tableGateway);
                },
                'HookerTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Hooker());
                    return new TableGateway('hooker', $dbAdapter, null, $resultSetPrototype);
                },
                        
                'Frontend\Model\UserHookerTable' => function($sm) {
                    $tableGateway = $sm->get('UserHookerTableGateway');
                    return new UserHookerTable($tableGateway);
                },
                'UserHookerTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserHooker());
                    return new TableGateway('user_hooker', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\BusinessTable' => function($sm) {
                    $tableGateway = $sm->get('BusinessTableGateway');
                    return new BusinessTable($tableGateway);
                },
                'BusinessTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Business());
                    return new TableGateway('business', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\UserBusinessTable' => function($sm) {
                    $tableGateway = $sm->get('UserBusinessTableGateway');
                    return new UserBusinessTable($tableGateway);
                },
                'UserBusinessTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserBusiness());
                    return new TableGateway('user_business', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\ArmTable' => function($sm) {
                    $tableGateway = $sm->get('ArmTableGateway');
                    return new ArmTable($tableGateway);
                },
                'ArmTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Arm());
                    return new TableGateway('arm', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\UserArmTable' => function($sm) {
                    $tableGateway = $sm->get('UserArmTableGateway');
                    return new UserArmTable($tableGateway);
                },
                'UserArmTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserArm());
                    return new TableGateway('user_arm', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\DrugTable' => function($sm) {
                    $tableGateway = $sm->get('DrugTableGateway');
                    return new DrugTable($tableGateway);
                },
                'DrugTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Drug());
                    return new TableGateway('drug', $dbAdapter, null, $resultSetPrototype);
                },
                        
                'Frontend\Model\RobberyTable' => function($sm) {
                    $tableGateway = $sm->get('RobberyTableGateway');
                    return new RobberyTable($tableGateway);
                },
                'RobberyTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Robbery());
                    return new TableGateway('robbery', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\UserDrugTable' => function($sm) {
                    $tableGateway = $sm->get('UserDrugTableGateway');
                    return new UserDrugTable($tableGateway);
                },
                'UserDrugTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserDrug());
                    return new TableGateway('user_drug', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Frontend\Model\MessageTable' => function($sm) {
                    $tableGateway = $sm->get('MessageTableGateway');
                    return new MessageTable($tableGateway);
                },
                'MessageTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Message());
                    return new TableGateway('message', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Thecrims\Pusher' => function($sm) {
                    $config = $this->getConfig();
                    return new Pusher($config['pusher']['host'], $config['pusher']['key']);
                },
                'AuthService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbAuthAdapter = new AuthAdapter($dbAdapter, 'user', 'user_username', 'user_password', 'user_active=1'); // "// MD5(?) || MD5(CONCAT('staticSalt', ?, password_salt))"
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbAuthAdapter);
                    //$authService->setStorage($sm->get('SanAuth\Model\MyAuthStorage'));
                    return $authService;
                },
            ),
        );
    }

}
