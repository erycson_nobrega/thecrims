<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use \Frontend\Model\UserHooker;
use \Frontend\Model\Business;

class HookersController extends AbstractActionController
{
    public $user;
    public $logger;
    public $UserTable;
    public $HookerTable;
    public $UserHookerTable;
    public $BusinessTable;
    public $UserBusinessTable;
    
    public function onDispatch(MvcEvent $event) {
        $this->logger = $this->getServiceLocator()->get('Log\App');
        $service = $this->getServiceLocator();
        
        $auth = $service->get('AuthService');
        if (!$auth->hasIdentity()) {      
            return $this->redirect()->toRoute('index');
        }
        
        $this->UserTable         = $service->get('Frontend\Model\UserTable');
        $this->HookerTable       = $service->get('Frontend\Model\HookerTable');
        $this->UserHookerTable   = $service->get('Frontend\Model\UserHookerTable');
        $this->BusinessTable     = $service->get('Frontend\Model\BusinessTable');
        $this->UserBusinessTable = $service->get('Frontend\Model\UserBusinessTable');
        $this->translator        = $service->get('Translator');
        $this->config            = $service->get('config');
        
        $this->user = $this->UserTable->getUser($auth->getIdentity());
        $this->layout()->setVariable('user', $this->user);
        $event->getTarget()->layout('layout/logged');
        
        parent::onDispatch($event);
    }
    
    public function indexAction()
    {
        $message = null;
        
        if ($this->getRequest()->isPost()) {
            $action = $this->params()->fromPost('action', null);
            if ($action == 'aceefbebfacf')
                $message = $this->buyHooker();
            else if ($action == 'bdbafcedaf')
                $message = $this->sellHooker();
        }
        
        // Venda (1 por venda): You sold {1} {Britney} for {$267}
        // Compra : You bought {1} {Dolly} for {$400}
        // Casa cheia: You can't handle more hookers, buy a whorehouse.
        // Colleta: You have collected $12685211<br><br>1 of your hookers has died in a horrible disease: Woman of Wonder
        
        return new ViewModel([
            'hookers' => $this->HookerTable->getAllForSell(),
            'user_hookers' => $this->UserHookerTable->getAllUserHookers($this->user->user_id),
            'free_space' => $this->getUserFreeSpace(),
            'message' => $message
        ]);
    }
    
    /**
     * Faz a validação da compra, como tambem faz o calculo do preço
     * 
     * @return array Mensagem resultante
     */
    private function buyHooker()
    {
        $params = $this->getRequest()->getPost()->toArray();
        unset($params['action']);
        
        // Pega as prostitutas que vão ser compradas
        $hookers = array_filter($params, function($id) {
            return !empty($id) && $id > 0;
        });
        
        // Calcula o preço total das prostitutas
        $hooker = null;         // Prostituta que está sendo comprada
        $hookerAmount = 0;      // Quantidade comprada da ultima prostituta
        $hookerPrice = 0;       // Preço da ultima prostituta
        $hookerPriceTotal = 0;  // Total a ser gasto na compra
        $hookerAmountTotal = 0; // Total de prostitutas sendo compradas
        
        // Valida a compra das prostitutas uma por uma
        foreach($hookers as $id => $amount) {
            $hooker = $this->HookerTable->getHooker($id);
            
            // Verifica se a prostituta existe e se está a venda
            if (!$hooker || !$hooker->hok_for_sell) {
                return [
                    'type'    => 'error',
                    'image'   => 'forbidden',
                    'message' => 'Something went wrong'
                ];
            }
            
            $hookerAmount = $amount;
            $hookerAmountTotal += $amount;
            
            $userHooker = $this->UserHookerTable->getUserHooker($this->user->user_id, $id);
            $userAmount = $userHooker ? $userHooker->uhok_amount : 0;
            
            // O calculo é feito com base:
            //     O valor base da prostituta
            //     A quantidade que o usuário já tem * o preço de soma de cada prostituta
            //     A quantidade que o usuário quer comprar * o preço de soma de cada prostituta
            $hookerPrice = $hooker->hok_price + ($userAmount * $hooker->hok_price_sum) + ($amount * $hooker->hok_price_sum);
            $hookerPriceTotal += $hookerPrice;
        }
        
        // Verifica se tem espaço livre
        if ($this->getUserFreeSpace() < $hookerAmountTotal) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'You can\'t handle more hookers, buy a whorehouse.'
            ];
        }
        // Verifica se tem dinheiro suficiente para comprar
        else if ($this->user->user_cash < $hookerPriceTotal) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Not enough cash'
            ];
        }
        // Compra a prostitura
        else {
            $this->saveHookers($hookers, $hookerPriceTotal);
            return [
                'type'    => 'ok',
                'image'   => 'check',
                'message' => sprintf('You bought %d %s for $%d', $hookerAmount, $this->translator->translate("hookers.name.{$hooker->hok_id}"), $hookerPrice)
            ];
        }
    }
    
    /**
     * Salva a compra no banco de dados
     * 
     * @param array $hookers Lista de prostitutas a serem compradas
     * @param float $price Custo total
     */
    private function saveHookers($hookers, $price)
    {
        foreach($hookers as $id => $amount) {
            $userHooker = $this->UserHookerTable->getUserHooker($this->user->user_id, $id);
            if (!$userHooker) {
                $userHooker          = new UserHooker();
                $userHooker->user_id = $this->user->user_id;
                $userHooker->hok_id  = $id;
            }
            $userHooker->uhok_amount = $amount;
            $this->UserHookerTable->saveUserHooker($userHooker);
        }
        
        $this->user->user_cash -= $price;
        $this->UserTable->saveUser($this->user);
    }
    
    /**
     * Calcula quantas prostitutas o usuário ainda pode ter
     * 
     * @return int Espaço livre
     */
    private function getUserFreeSpace()
    {
        $freeSpace  = 5;
        $businesses = $this->UserBusinessTable->getUserBusinessByType($this->user->user_id, Business::TYPE_WHOREHOUSE);
        
        foreach($businesses as $business) {
            $Business   = $this->BusinessTable->getBusiness($business->bus_id);
            $freeSpace += $business->uhok_amount * $Business->bus_capacity;
        }
        
        return $freeSpace - $this->getUserHookersTotal();
    }
    
    /**
     * Calcula quantas prostitutas o usuário já possui
     * 
     * Obs.: É necessario colocar uma transaction
     * 
     * @return int Total de Prostitutas
     */
    private function getUserHookersTotal()
    {
        $hookers = $this->UserHookerTable->getAllUserHookers($this->user->user_id);
        $total   = 0;
        
        foreach($hookers as $hooker) {
            $total += $hooker->uhok_amount;
        }
        
        return $total;
    }
    
    private function sellHooker()
    {
        $params = $this->getRequest()->getPost()->toArray();
        
        // Verifica se a prostituta existe e se está a venda
        if (!isset($params['hookertypes'])) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        
        
        $hookertypes = $params['hookertypes'];
        unset($params['action']);
        unset($params['hookertypes']);
        
        // Pega as prostitutas que vão ser vendidas e verifica
        // quantos estão sendo vendidos
        $hookers = [];
        foreach($hookertypes as $hooker_id) {
            $hooker_id = (int) $hooker_id;
            
            if (empty($hooker_id) || $hooker_id <= 0 || !isset($params[$hooker_id])) {
                continue;
            }
            $hookers[$hooker_id] = (int) $params[$hooker_id];
        }
        unset($params);
        
        // Valida a compra das prostitutas uma por uma
        $messages = [];
        foreach($hookers as $id => $amount) {
            $hooker = $this->HookerTable->getHooker($id);
            $userHooker = $this->UserHookerTable->getUserHooker($this->user->user_id, $id);
            
            // Verifica se a prostituta existe e se está a venda
            if (!$hooker || !$userHooker) {
                return [
                    'type'    => 'error',
                    'image'   => 'forbidden',
                    'message' => 'Something went wrong'
                ];
            }
            
            $userHooker->uhok_amount -= $amount;
            // Verifica se a prostituta existe e se está a venda
            if ($userHooker->uhok_amount < 0) {
                return [
                    'type'    => 'error',
                    'image'   => 'forbidden',
                    'message' => 'Something went wrong'
                ];
            }
            
            $this->user->user_cash += $amount * $hooker->hok_price / 3;
            
            if ($userHooker->uhok_amount == 0) {
                $this->UserHookerTable->deleteHooker($userHooker->uhok_id);
            } else {
                $this->UserHookerTable->saveUserHooker($userHooker);
            }
            
            $messages[] = [
                'type'    => 'ok',
                'image'   => 'check',
                'message' => sprintf("You bought %d %s for $%s",
                    $amount,
                    $this->translator->translate("hookers.name.{$hooker->hok_id}"),
                    number_format($amount * $hooker->hok_price / 3)
                )
            ];
        }
        
        $this->UserTable->saveUser($this->user);
        
        return $messages;
    }
}
