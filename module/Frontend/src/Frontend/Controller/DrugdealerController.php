<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\MvcEvent;
use \Frontend\Model\UserDrug;

class DrugdealerController extends AbstractActionController {

    public $user;
    public $config;
    public $DrugTable;
    public $UserDrugTable;
    public $Pusher;
    public $translator;

    public function onDispatch(MvcEvent $event) {
        $service = $this->getServiceLocator();

        $auth = $service->get('AuthService');
        if (!$auth->hasIdentity()) {
            return $this->redirect()->toRoute('index');
        }

        $this->UserTable     = $service->get('Frontend\Model\UserTable');
        $this->DrugTable     = $service->get('Frontend\Model\DrugTable');
        $this->UserDrugTable = $service->get('Frontend\Model\UserDrugTable');
        $this->Pusher        = $service->get('Thecrims\Pusher');
        $this->translator    = $service->get('Translator');
        $this->config        = $service->get('config');
        $this->user          = $this->UserTable->getUser($auth->getIdentity());
        
        $this->layout()->setVariable('user', $this->user); // aqui manda a variavel para o layout
        $event->getTarget()->layout('layout/logged');

        parent::onDispatch($event);
    }

    public function indexAction() {
        $message = null;
        
        if ($this->getRequest()->isPost()) {
            $action = $this->params()->fromPost('action', null);
            if ($action == 'ddcaebccac') {
                $message = $this->buyDrug();
            } else if ($action == 'eabcafdcafadbdbca') {
                $message = $this->sellDrug();
            }
        }
        
        return new ViewModel([
            'user'       => $this->user, // aqui manda pra view
            'drugs'      => $this->DrugTable->fetchAll(),
            'user_drugs' => $this->UserDrugTable->getAllUserDrugs($this->user->user_id),
            'message'    => $message
        ]);
    }

    public function getStockQuantitiesAction() {
        $json  = [];
        $drugs = $this->DrugTable->fetchAll();

        foreach ($drugs as $drugs) {
            $json[] = ['q' => (int) $drugs['drug_stock'], 'id' => md5($this->config['site']['secret-key'] . $drugs['drug_id'])];
        }

        return new JsonModel($json);
    }

    private function buyDrug() {
        $drug_id = $this->params()->fromPost('id', null);
        $quantity = (int) $this->params()->fromPost('quantity', 0);

        $drug = $this->DrugTable->getDrug($drug_id);
        if ($this->user->user_stamina < 5) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translator->translate("geral.nostamina"))
            ];
        } else if ($this->user->user_business_day == 0) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translator->translate("drugs.notickets"))
            ];
        } else if (!$drug || $drug->drug_stock < $quantity) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translator->translate("drugs.nostock"))
            ];
        } else if (ceil($drug->drug_price_buy * $quantity) > $this->user->user_cash) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translator->translate("geral.nocash"))
            ];
        } else if ($quantity == 0) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translator->translate("drugs.noqtde"))
            ];
        }

        $userDrug = $this->UserDrugTable->getUserDrug($this->user->user_id, $drug->drug_id);
        if (!$userDrug) {
            $userDrug          = new UserDrug();
            $userDrug->user_id = $this->user->user_id;
            $userDrug->drug_id = $drug->drug_id;
        }

        $drug->drug_stock -= $quantity;
        $this->DrugTable->saveDrug($drug);

        $userDrug->udrug_stock += $quantity;
        $this->UserDrugTable->saveUserDrug($userDrug);

        $this->user->user_cash         -= ceil($drug->drug_price_buy * $quantity);
        $this->user->user_stamina      = $this->user->user_stamina - 5;
        $this->user->user_business_day = $this->user->user_business_day-1;
        $this->UserTable->saveUser($this->user);

        $this->Pusher->trigger('drugdealer', 'updatedrugdealerstock');


        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => sprintf($this->translator->translate("drugs.buy"), ceil($drug->drug_price_buy), $quantity, $this->translator->translate("drugs.name.{$drug->drug_id}"))
        ];
    }

    private function sellDrug() {
        $drug_id  = $this->params()->fromPost('id', null);
        $quantity = (int) $this->params()->fromPost('quantity', 0);

        if ($quantity < 1) {
            return null;
        }

        $userDrug = $this->UserDrugTable->getUserDrug($this->user->user_id, $drug_id);
        if (!$userDrug) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        } else if ($quantity > $userDrug->udrug_stock) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Not enough haschi! You can´t sell what ya don´t have mon!'
            ];
        }

        $drug = $this->DrugTable->getDrug($drug_id);

        $userDrug->udrug_stock -= $quantity;
        $this->UserDrugTable->saveUserDrug($userDrug);

        $this->user->user_cash += ceil($drug->drug_price_sell * $quantity);
        $this->UserTable->saveUser($this->user);

        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => sprintf('You have earned $%d on %d %s', ceil($drug->drug_price_sell * $quantity), $quantity, $this->translator->translate("drugs.name.{$drug->drug_id}"))
        ];
    }

}
