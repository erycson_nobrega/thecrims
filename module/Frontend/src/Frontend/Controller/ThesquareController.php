<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\MvcEvent;

use Frontend\Model\Message;

class ThesquareController extends AbstractActionController
{
    public $user;
    public $logger;
    public $UserTable;
    public $MessageTable;
    public $Pusher;
    public $translator;
    
    public function onDispatch(MvcEvent $event)
    {
        $service = $this->getServiceLocator();
        
        $auth = $service->get('AuthService');
        if (!$auth->hasIdentity()) {      
            return $this->redirect()->toRoute('index');
        }
        
        $this->UserTable    = $service->get('Frontend\Model\UserTable');
        $this->MessageTable = $service->get('Frontend\Model\MessageTable');
        $this->Pusher       = $service->get('Thecrims\Pusher');
        $this->translator   = $service->get('Translator');
        $this->config       = $service->get('config');
        
        $this->user = $this->UserTable->getUser($auth->getIdentity());
        $this->layout()->setVariable('user', $this->user);
        $event->getTarget()->layout('layout/logged');
        
        parent::onDispatch($event);
    }
    
    public function indexAction()
    {
        if ($this->getRequest()->isPost()) {
            $action = $this->params()->fromPost('action', null);
            if ($action == 'dfadbaee') {
                return $this->postMessage();
            }
        }
        
        return new ViewModel([
            'messages' => $this->MessageTable->getLastMessageByType(Message::TYPE_SQUARE)
        ]);
    }
    
    private function postMessage()
    {
        $text = trim($this->params()->fromPost('message', ''));
        
        if (strlen($text) == 0) {
            return new JsonModel('The message can not be empty');
        } else if (strlen($text) > 100) {
            return new JsonModel('Maximum size of 100 characters');
        }
        $text = nl2br(htmlentities($text));
        
        $message = new Message();
        $message->msg_day  = 1;
        $message->msg_hour = date('H:i:s');
        $message->msg_type = Message::TYPE_SQUARE;
        $message->user_id  = $this->user->user_id;
        $message->msg_text = $text;
        $this->MessageTable->saveMessage($message);
        
        $htmlMessage = sprintf('[%d %s] &lt;<a href="profile/%d"><span class="nicktext">%s</span></a>&gt; %s<br>',
            1,
            date('H:i'),
            $this->user->user_id,
            $this->user->user_username,
            $text
        );
        $this->Pusher->trigger('chat', 'chatmessage', $htmlMessage);
        
        return new JsonModel('ok');
    }
}