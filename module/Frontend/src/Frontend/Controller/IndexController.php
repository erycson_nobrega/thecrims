<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Frontend\Form\LoginForm;

class IndexController extends AbstractActionController
{
    public function onDispatch(MvcEvent $event) {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {      
            return $this->redirect()->toRoute('start');
        }
        return parent::onDispatch($event);
    }
    
    public function indexAction()
    {
        return new ViewModel(array(
            'loginForm'     => new LoginForm(),
            'flashMessages' => $this->flashMessenger()->getMessages()
        ));
    }
}
