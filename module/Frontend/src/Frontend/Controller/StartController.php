<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class StartController extends AbstractActionController
{
    public function onDispatch(MvcEvent $event) {
        $auth = $this->getServiceLocator()->get('AuthService');
        if (!$auth->hasIdentity()) {      
            return $this->redirect()->toRoute('index');
        }
        
        $userTable = $this->getServiceLocator()->get('Frontend\Model\UserTable');
        $this->layout()->setVariable('user', $userTable->getUser($auth->getIdentity()));
        $event->getTarget()->layout('layout/logged');
        parent::onDispatch($event);
    }
    
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function firstloginAction()
    {
        return new ViewModel();
    }
    
    public function choosecharacterAction()
    {
        return new ViewModel();
    }
    
    public function benefitsAction()
    {
        return new ViewModel();
    }
}
