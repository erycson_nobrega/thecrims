<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Frontend\Model\Arm;
use Frontend\Model\ArmTable;
use Frontend\Model\UserArm;
use Frontend\Model\UserArmTable;

class ArmsdealerController extends AbstractActionController
{
    public $user;
    public $logger;
    public $ArmTable;
    public $UserTable;
    public $UserArmTable;
    public $translator;
    
    public function onDispatch(MvcEvent $event)
    {
        $service = $this->getServiceLocator();
        $auth    = $service->get('AuthService');
        if (!$auth->hasIdentity()) {      
            return $this->redirect()->toRoute('index');
        }
        
        $this->UserTable    = $service->get('Frontend\Model\UserTable');
        $this->ArmTable     = $service->get('Frontend\Model\ArmTable');
        $this->UserArmTable = $service->get('Frontend\Model\UserArmTable');
        $this->translator   = $service->get('Translator');
        $this->config       = $service->get('config');
        
        $this->user = $this->UserTable->getUser($auth->getIdentity());
        $this->layout()->setVariable('user', $this->user);
        $event->getTarget()->layout('layout/logged');
        
        parent::onDispatch($event);
    }
    
    public function indexAction()
    {
        $message = null;
        $section = $this->params()->fromRoute('section', null);
        $section = $section == Arm::SECTION_ARMOR ? Arm::SECTION_ARMOR : Arm::SECTION_WEAPON;
        
        if ($this->getRequest()->isPost()) {
            $action = $this->params()->fromPost('action', null);
            switch ($action) {
                case 'buy_weapon':  $message = $this->buyWeapon(); break;
                case 'sell_weapon': $message = $this->sellWeapon(); break;
                case 'buy_armor':   $message = $this->buyArmor(); break;
                case 'sell_armor':  $message = $this->sellArmor(); break;
            }
        }
        
        return new ViewModel([
            'message' => $message,
            'section' => $section,
            'user' => $this->user,
            'user_arms' => $this->UserArmTable->getUserArmsBySection($this->user->user_id, $section),
            'arms' => $this->ArmTable->getAllBySection($section)
        ]);
    }
    
    public function buyWeapon()
    {
        $weapon_id = $this->params()->fromPost('id', null);
        $section   = $this->params()->fromPost('section', null);
        $quantity  = (int) $this->params()->fromPost('quantity', 1);
        
        $weapon = $this->ArmTable->getArm($weapon_id);
        
        // Verifica se é uma arma
        if ($section != Arm::SECTION_WEAPON) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        // Verifica se é uma arma
        else if (!$weapon || $weapon->arm_section != Arm::SECTION_WEAPON) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        // Verifica se é uma arma
        else if ($quantity < 0 || $quantity == NULL) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        
        $quantity = $quantity ? $quantity : 1;
        $total    = $weapon->arm_cash * $quantity;
        $userArm  = new UserArm();
        $userArm->exchangeArray([
            'user_id' => $this->user->user_id, 
            'arm_id' => $weapon->arm_id, 
            'equipped' => 0
        ]);
        
        if ($total > $this->user->user_cash) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Not enough cash'
            ];
        }
        
        while ($quantity--) {
            $this->UserArmTable->saveUserArm($userArm);
        }
        $this->user->user_cash -= $total;
        $this->UserTable->saveUser($this->user);
        
        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => sprintf('You have spent $%d on a %s', $total, $this->translator->translate("arms.name.{$weapon->arm_id}"))
        ];
    }
    
    public function sellWeapon()
    {
        $section = $this->params()->fromPost('section', null);
        $uarm_id = $this->params()->fromPost('id', 0);
        $weapon  = $this->UserArmTable->getUserArmByID($uarm_id);
        
        if ($section != Arm::SECTION_WEAPON) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        else if (!$weapon || $weapon['arm_section'] != Arm::SECTION_WEAPON) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        else if ($weapon['user_id'] != $this->user->user_id) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        
        $this->UserArmTable->deleteArm($uarm_id);
        $this->user->user_cash -= $weapon['arm_cash'] / 2.2;
        $this->UserTable->saveUser($this->user);
        
        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => sprintf('You have sold your %s for $%d', $this->translator->translate("arms.name.{$weapon['arm_id']}"), $weapon['arm_cash'] / 2.2)
        ];
    }
    
    public function buyArmor()
    {
        $weapon_id = $this->params()->fromPost('id', null);
        $section   = $this->params()->fromPost('section', null);
        $quantity  = (int) $this->params()->fromPost('quantity', 1);
        $armor     = $this->ArmTable->getArm($weapon_id);
        
        // Verifica se é uma arma
        if ($section != Arm::SECTION_ARMOR) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        // Verifica se é uma arma
        else if (!$armor || $armor->arm_section != Arm::SECTION_ARMOR) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        // Verifica se é uma arma
        else if ($quantity < 0) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        // Verifica se é uma arma
        else if ($this->user->user_level < $armor->arm_required_level) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => sprintf($this->translate('arms.required.level'), $armor->arm_required_level)
            ];
        }
        
        $quantity = $quantity ? $quantity : 1;
        $total    = $armor->arm_cash * $quantity;
        $userArm  = new UserArm();
        $userArm->exchangeArray([
            'user_id'  => $this->user->user_id, 
            'arm_id'   => $armor->arm_id, 
            'equipped' => 0
        ]);
        
        if ($total > $this->user->user_cash) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Not enough cash'
            ];
        }
        
        while ($quantity--) {
            $this->UserArmTable->saveUserArm($userArm);
        }
        
        $this->user->user_cash -= $total;
        $this->UserTable->saveUser($this->user);
        
        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => 'You bought a new armor.'
        ];
    }
    
    public function sellArmor()
    {
        $section = $this->params()->fromPost('section', null);
        $uarm_id = $this->params()->fromPost('id', 0);
        
        $armor = $this->UserArmTable->getUserArmByID($uarm_id);
        
        if ($section != Arm::SECTION_ARMOR) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        else if (!$armor || $armor['arm_section'] != Arm::SECTION_ARMOR) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        else if ($armor['user_id'] != $this->user->user_id) {
            return [
                'type'    => 'error',
                'image'   => 'forbidden',
                'message' => 'Something went wrong'
            ];
        }
        
        $this->UserArmTable->deleteArm($uarm_id);
        $this->user->user_cash -= $armor['arm_cash'] / 2.2;
        $this->UserTable->saveUser($this->user);
        
        return [
            'type'    => 'ok',
            'image'   => 'check',
            'message' => 'You sold an armor.'
        ];
    }
}
