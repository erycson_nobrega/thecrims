<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\Result as Authentication;
use Frontend\Form\LoginForm;

class LoginController extends AbstractActionController {
    private $authservice;
    
    /**
     * 
     * @return boolean
     * @see https://samsonasik.wordpress.com/2013/05/29/zend-framework-2-working-with-authenticationservice-and-db-session-save-handler/
     */
    public function indexAction() {
        $form     = new LoginForm();
        $username = $this->params()->fromPost('username', null);
        $password = $this->params()->fromPost('password', null);

        $request = $this->getRequest();
        if (!$request->isPost())
            return false;

        $form->setData($request->getPost());
        if (!$form->isValid())
            return false;

        $this->getAuthService()
             ->getAdapter()
             ->setIdentity($username)
             ->setCredential($password);
        $service = $this->getAuthService();
        $result = $service->authenticate();
        
        if ($result->isValid()) {
            $resultRow = $service->getAdapter()->getResultRowObject();
            $service->getStorage()->write((int) $resultRow->user_id);
            return $this->redirect()->toRoute('start');
        } else {
            switch ($result->getCode()) {
                case Authentication::FAILURE_IDENTITY_NOT_FOUND:
                    $this->flashMessenger()->addMessage('This user doesnt exist or has been deleted.');
                    break;
                case Authentication::FAILURE_CREDENTIAL_INVALID:
                    $this->flashMessenger()->addMessage('Wrong username or password<br/> If you\'ve lost your password click on Lost password under the login box to the left. Your account will be locked for 1 hour after 5 unsuccessful login attempts.');
                    break;
                default:
                    $this->flashMessenger()->addMessage('Erro desconhecido');
                    break;
            }
            return $this->redirect()->toRoute('index');
        }
    }

    public function getAuthService() {
        if (!isset($this->authservice)) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }

        return $this->authservice;
    }

}
