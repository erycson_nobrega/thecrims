<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\MvcEvent;
use Frontend\Model\RobberyTable;


class RobberyController extends AbstractActionController {

    public $user;
    public $config;
    //public $DrugTable;
    //public $UserDrugTable;
    public $Pusher;
    public $translator;
    public $RobberyTable;

    public function onDispatch(MvcEvent $event) {
        $service = $this->getServiceLocator();

        $auth = $service->get('AuthService');
        if (!$auth->hasIdentity()) {
            return $this->redirect()->toRoute('index');
        }

        $this->UserTable    = $service->get('Frontend\Model\UserTable');
        $this->DrugTable    = $service->get('Frontend\Model\DrugTable');
        //$this->UserDrugTable = $service->get('Frontend\Model\UserDrugTable');
        $this->RobberyTable = $service->get('Frontend\Model\RobberyTable');
        $this->Pusher       = $service->get('Thecrims\Pusher');
        $this->translator   = $service->get('Translator');
        $this->config       = $service->get('config');

        $this->user = $this->UserTable->getUser($auth->getIdentity());
        $this->layout()->setVariable('user', $this->user); // aqui manda a variavel para o layout
        $event->getTarget()->layout('layout/logged');

        parent::onDispatch($event);
    }

    public function indexAction() {
        $message = null;

        if ($this->getRequest()->isPost()) {
            $action = $this->params()->fromPost('action', null);
            if ($action == 'ddcaebccac') {
                $message = $this->buyDrug();
            } else if ($action == 'eabcafdcafadbdbca') {
                $message = $this->sellDrug();
            }
        }

        return new ViewModel([
            'user'     => $this->user, // aqui manda pra view
            'drugs'    => $this->DrugTable->fetchAll(),
            //'user_drugs' => $this->UserDrugTable->getAllUserDrugs($this->user->user_id),
            'robbery'  => $this->RobberyTable->fetchAll(),
            'robberys' => $this->getRobberys(),
            'message'  => $message
        ]);
    }
    
    public function getRobberys() {
        $json = [];
        $robberys = $this->RobberyTable->fetchAll();

        /*
                var pos = {
                    translatedname: 'aa',
                    energy: '5',
                    //spirit: '5',
                    spiritname: 'Very low',
                    difficulty: '3',
                    min_reward: '1',
                    max_reward: '10',
                    successprobability: '100',
                    successprobability_image: '/static/images/icons/dot_green.PNG'
                }
         * 
         */
        foreach ($robberys as $robbery) {//ele estava funcionando antes de mudar esta função que vc fez
            $json[] = [
                'id' => $robbery['robbery_id'],
                'translatedname' => $this->translator->translate("robbery.name.1"), 
                'difficulty' => $robbery['robbery_difficult'],
                'energy' => $robbery['robbery_stamina'],
                'spirit' => '',
                'spiritname' => '',
                'type' => '',
                'translatedname' => '',
                'max_reward' => $robbery['robbery_max_reward'],
                'min_reward' => $robbery['robbery_min_reward'],
                'requiredstats' => '',
                'user_power' => '',
                'successprobability' => '',
                'successprobability_image' => '/static/images/icons/dot_green.PNG',
            ];
        }

        return $json;
    }

}
