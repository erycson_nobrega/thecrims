<?php

namespace Frontend\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'POST');
        $this->setAttribute('action', 'login');
        $this->setAttribute('id', 'loginform');
        $this->setAttribute('class', 'form-signin');

        $this->add([
            'name' => 'username',
            'type' => 'Text',
            'attributes' => [
                'size' => 19,
                'placeholder' => 'Username',
                'maxlength' => 30,
                'class' => 'front-input'
            ],
            'validators' => [['name' => 'NotEmpty']],
        ]);
        
        $this->add([
            'name' => 'password',
            'type' => 'Password',
            'attributes' => [
                'size' => 19,
                'placeholder' => 'Password',
                'maxlength' => 40,
                'class' => 'front-input'
            ],
            'validators' => [['name' => 'NotEmpty']],
        ]);
        
        $this->add([
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'value' => 'Login',
                'class' => 'btn btn-large btn-inverse',
            ],
        ]);
        
        $this->add([
            'name' => 'env',
            'type' => 'Hidden',
        ]);
        $this->add([
            'name' => 'pl',
            'type' => 'Hidden',
        ]);
        $this->add([
            'name' => 'action',
            'type' => 'Hidden',
            'attributes' => ['value' => 'dfeedacbdfebfcd']
        ]);
    }

}
