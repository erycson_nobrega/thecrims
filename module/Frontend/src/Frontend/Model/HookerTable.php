<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;

class HookerTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }

    /**
     * Retorna todos as prostitutas que estão a venda
     * 
     * @return array
     */
    public function getAllForSell() {
        return $this->tableGateway->select(array('hok_for_sell' => 1));
    }

    public function getHooker($id) {
        $rowset = $this->tableGateway->select(array('hok_id' => $id));
        $row    = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveHooker(Hooker $hok) {
        $data = array(
            'hok_id'    => $hok->hok_id,
            'hok_price' => $hok->hok_price,
            'hok_image' => $hok->hok_image,
        );

        $id = (int) $hok->hok_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getHooker($id)) {
                $this->tableGateway->update($data, array('hok_id' => $id));
            } else {
                throw new \Exception('Hooker id does not exist');
            }
        }
    }

    public function deleteHooker($id) {
        $this->tableGateway->delete(array('hok_id' => (int) $id));
    }

}
