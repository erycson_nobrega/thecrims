<?php

namespace Frontend\Model;

class Business {
    const TYPE_WHOREHOUSE = 'whorehouse';
    const TYPE_NIGHTLIFE = 'nightlife';
    
    public $bus_id;
    public $bus_type;
    public $bus_capacity;
    public $bus_price;
    public $bus_image;
    
    public function exchangeArray($data)
    {
         $this->bus_id       = (!empty($data['bus_id'])) ? $data['bus_id'] : null;
         $this->bus_type     = (!empty($data['bus_type'])) ? $data['bus_type'] : null;
         $this->bus_capacity = (!empty($data['bus_capacity'])) ? $data['bus_capacity'] : null;
         $this->bus_price    = (!empty($data['bus_price'])) ? $data['bus_price'] : null;
         $this->bus_image    = (!empty($data['bus_image'])) ? $data['bus_image'] : null;
    }
}
