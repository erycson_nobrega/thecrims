<?php

namespace Frontend\Model;

class Hooker {
    public $hok_id;
    public $hok_payout;
    public $hok_price;
    public $hok_price_sum;
    public $hok_image;
    public $hok_for_sell;
    
    public function exchangeArray($data)
    {
         $this->hok_id        = (!empty($data['hok_id'])) ? $data['hok_id'] : null;
         $this->hok_payout    = (!empty($data['hok_payout'])) ? $data['hok_payout'] : null;
         $this->hok_price     = (!empty($data['hok_price'])) ? $data['hok_price'] : null;
         $this->hok_price_sum = (!empty($data['hok_price_sum'])) ? $data['hok_price_sum'] : null;
         $this->hok_image     = (!empty($data['hok_image'])) ? $data['hok_image'] : null;
         $this->hok_for_sell  = (!empty($data['hok_for_sell'])) ? $data['hok_for_sell'] : null;
    }
}
