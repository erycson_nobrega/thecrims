<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class MessageTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getMessage($id) {
        $rowset = $this->tableGateway->select(['msg_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function getAllByType($type) {
        return $this->tableGateway->select(['msg_type' => $type]);
    }

    public function getLastMessageByType($type) {
        $select = $this->tableGateway->select(function(Select $select) use ($type) {
            $select->join('user', 'user.user_id=message.user_id');
            $select->where(['msg_type' => $type]);
            $select->order('msg_id DESC');
            $select->limit(35);
        });
        return $select->getDataSource();
    }

    public function saveMessage(Message $msg) {
        $data = [
            'msg_id'   => $msg->msg_id,
            'user_id'  => $msg->user_id,
            'msg_type' => $msg->msg_type,
            'msg_day'  => $msg->msg_day,
            'msg_hour' => $msg->msg_hour,
            'msg_text' => $msg->msg_text,
        ];

        $id = (int) $msg->msg_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getMessage($id)) {
                $this->tableGateway->update($data, ['msg_id' => $id]);
            } else {
                throw new \Exception('Message id does not exist');
            }
        }
    }

    public function deleteMessage($id) {
        $this->tableGateway->delete(['msg_id' => $id]);
    }

}
