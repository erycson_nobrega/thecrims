<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UserHookerTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAllUserHookers($user_id) {
        $select = $this->tableGateway->select(function(Select $select) use ($user_id) {
            $select->join(['h' => 'hooker'], 'h.hok_id=user_hooker.hok_id');
            $select->where(['user_hooker.user_id' => $user_id]);
        });
        
        $results = $select->getDataSource();
        $hookers = [];
        
        foreach($results as $result) {
            $hookers[$result['hok_id']] = (object) $result;
        }
        
        return $hookers;
    }

    public function getUserHooker($user_id, $hooker_id)
    {
        $rowset = $this->tableGateway->select(['user_id' => $user_id, 'hok_id' => $hooker_id]);
        return $rowset->current();
    }

    public function getUserHookerById($id)
    {
        $rowset = $this->tableGateway->select(['uhok_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveUserHooker(UserHooker $uhok)
    {
        $data = [
            'uhok_id'     => $uhok->uhok_id,
            'user_id'     => $uhok->user_id,
            'hok_id'      => $uhok->hok_id,
            'uhok_amount' => $uhok->uhok_amount,
        ];

        $id = (int) $uhok->uhok_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            $userHooker = $this->getUserHookerById($id);
            // Se já existir, soma a quantidade com a que tem no banco de dados
            if ($userHooker) {
                $data['uhok_amount'] += $userHooker->amount;
                $this->tableGateway->update($data, ['uhok_id' => $id]);
            } else {
                throw new \Exception('UserHooker does not exist');
            }
        }
    }

    public function deleteHooker($id)
    {
        $this->tableGateway->delete(['uhok_id' => $id]);
    }

}
