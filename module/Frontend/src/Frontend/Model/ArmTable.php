<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;

class ArmTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getAllBySection($section) {
        return $this->tableGateway->select(['arm_section' => $section]);
    }

    public function getArm($id) {
        $rowset = $this->tableGateway->select(['arm_id' => $id]);
        $row    = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveArm(Arm $user) {
        $data = [
            'arm_id'        => $user->arm_id,
            'arm_type'      => $user->arm_type,
            'arm_strength'  => $user->arm_strength,
            'arm_tolerance' => $user->arm_tolerance,
            'arm_cash'      => $user->arm_cash,
            'arm_credits'   => $user->arm_credits,
            'arm_image'     => $user->arm_image,
        ];

        $id = (int) $user->arm_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getArm($id)) {
                $this->tableGateway->update($data, ['arm_id' => $id]);
            } else {
                throw new \Exception('Arm id does not exist');
            }
        }
    }

    public function deleteArm($id) {
        $this->tableGateway->delete(['arm_id' => $id]);
    }

}
