<?php

namespace Frontend\Model;

class UserDrug {
    public $udrug_id;
    public $user_id;
    public $drug_id;
    public $udrug_stock;
    
    public function exchangeArray($data)
    {
         $this->udrug_id    = (!empty($data['udrug_id'])) ? $data['udrug_id'] : null;
         $this->user_id     = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->drug_id     = (!empty($data['drug_id'])) ? $data['drug_id'] : null;
         $this->udrug_stock = (!empty($data['udrug_stock'])) ? $data['udrug_stock'] : 0;
    }
}
