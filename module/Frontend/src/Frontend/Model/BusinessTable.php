<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;

class BusinessTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getBusiness($id) {
        $rowset = $this->tableGateway->select(['bus_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveBusiness(Business $user) {
        $data = array(
            'bus_id'       => $user->bus_id,
            'bus_type'     => $user->bus_type,
            'bus_capacity' => $user->bus_capacity,
            'bus_price'    => $user->bus_price,
            'bus_image'    => $user->bus_image,
        );

        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getBusiness($id)) {
                $this->tableGateway->update($data, ['bus_id' => $id]);
            } else {
                throw new \Exception('Business id does not exist');
            }
        }
    }

    public function deleteBusiness($id) {
        $this->tableGateway->delete(['bus_id' => $id]);
    }

}
