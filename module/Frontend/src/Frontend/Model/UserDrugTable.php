<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UserDrugTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAllUserDrugs($user_id) {
        $select = $this->tableGateway->select(function(Select $select) use ($user_id) {
            $select->join(['d' => 'drug'], 'd.drug_id=user_drug.drug_id');
            $select->where(['user_drug.user_id' => $user_id]);
        });
        
        return $select->getDataSource();
    }

    public function getUserDrug($user_id, $drug_id)
    {
        $rowset = $this->tableGateway->select(['user_id' => $user_id, 'drug_id' => $drug_id]);
        return $rowset->current();
    }

    public function getUserDrugById($id)
    {
        $rowset = $this->tableGateway->select(['udrug_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveUserDrug(UserDrug $drug)
    {
        $data = [
            'udrug_id'    => $drug->udrug_id,
            'user_id'     => $drug->user_id,
            'drug_id'     => $drug->drug_id,
            'udrug_stock' => $drug->udrug_stock,
        ];

        $id = (int) $drug->udrug_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            $userDrug = $this->getUserDrugById($id);
            // Se já existir, soma a quantidade com a que tem no banco de dados
            if ($userDrug) {
                $this->tableGateway->update($data, ['udrug_id' => $id]);
            } else {
                throw new \Exception('UserDrug does not exist');
            }
        }
    }

    public function deleteDrug($id)
    {
        $this->tableGateway->delete(['udrug_id' => $id]);
    }

}
