<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class DrugTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        $select = $this->tableGateway->select(function(Select $select) {
            $select->order('drug_price_buy ASC');
        });
        return $select->getDataSource();
    }

    public function getDrug($id) {
        $rowset = $this->tableGateway->select(['drug_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveDrug(Drug $drug) {
        $data = [
            'drug_id'         => $drug->drug_id,
            'drug_price_buy'  => $drug->drug_price_buy,
            'drug_price_sell' => $drug->drug_price_sell,
            'drug_stock'      => $drug->drug_stock,
        ];

        $id = (int) $drug->drug_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getDrug($id)) {
                $this->tableGateway->update($data, ['drug_id' => $id]);
            } else {
                throw new \Exception('Arm id does not exist');
            }
        }
    }

    public function deleteDrug($id) {
        $this->tableGateway->delete(['drug_id' => $id]);
    }

}
