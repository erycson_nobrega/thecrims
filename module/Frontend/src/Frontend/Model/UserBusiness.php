<?php

namespace Frontend\Model;

class UserBusiness {
    public $ubus_id;
    public $user_id;
    public $bus_id;
    public $ubus_amount;
    
    public function exchangeArray($data)
    {
         $this->ubus_id     = (!empty($data['ubus_id'])) ? $data['ubus_id'] : null;
         $this->user_id     = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->bus_id      = (!empty($data['bus_id'])) ? $data['bus_id'] : null;
         $this->ubus_amount = (!empty($data['ubus_amount'])) ? $data['ubus_amount'] : null;
    }
}
