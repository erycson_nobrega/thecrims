<?php

namespace Frontend\Model;

class User {
    public $user_id;
    public $user_username;
    public $user_password;
    public $user_email;
    public $user_info_accepted;
    public $user_country;
    public $user_date_register;
    public $user_date_last_login;
    public $user_date_of_birth;
    public $user_profile_visitores;
    public $user_time_online;
    public $user_times_banned;
    public $user_active;
    public $user_profession;
    public $user_stamina;
    public $user_addiction;
    public $user_respect;
    public $user_genere;
    public $user_kills;
    public $user_deads;
    public $user_level;
    public $user_spirit;
    public $user_intelligence;
    public $user_strength;
    public $user_charisma;
    public $user_tolerance;
    public $user_cash;
    public $user_credits;
    public $user_points_inventory;
    public $user_points_gang_donation;
    public $user_business_day;
    
    public function exchangeArray($data)
    {
         $this->user_id                   = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->user_username             = (!empty($data['user_username'])) ? $data['user_username'] : null;
         $this->user_password             = (!empty($data['user_password'])) ? $data['user_password'] : null;
         $this->user_email                = (!empty($data['user_email'])) ? $data['user_email'] : null;
         $this->user_info_accepted        = (!empty($data['user_info_accepted'])) ? $data['user_info_accepted'] : null;
         $this->user_country              = (!empty($data['user_country'])) ? $data['user_country'] : null;
         $this->user_date_register        = (!empty($data['user_date_register'])) ? $data['user_date_register'] : null;
         $this->user_date_last_login      = (!empty($data['user_date_last_login'])) ? $data['user_date_last_login'] : null;
         $this->user_date_of_birth        = (!empty($data['user_date_of_birth'])) ? $data['user_date_of_birth'] : null;
         $this->user_profile_visitores    = (!empty($data['user_profile_visitores'])) ? $data['user_profile_visitores'] : 0;
         $this->user_time_online          = (!empty($data['user_time_online'])) ? $data['user_time_online'] : 0;
         $this->user_times_banned         = (!empty($data['user_times_banned'])) ? $data['user_times_banned'] : 0;
         $this->user_active               = (!empty($data['user_active'])) ? $data['user_active'] : null;
         $this->user_profession           = (!empty($data['user_profession'])) ? $data['user_profession'] : null;
         $this->user_stamina              = (!empty($data['user_stamina'])) ? $data['user_stamina'] : null;
         $this->user_addiction            = (!empty($data['user_addiction'])) ? $data['user_addiction'] : 0;
         $this->user_respect              = (!empty($data['user_respect'])) ? $data['user_respect'] : 0;
         $this->user_genere               = (!empty($data['user_genere'])) ? $data['user_genere'] : null;
         $this->user_kills                = (!empty($data['user_kills'])) ? $data['user_kills'] : 0;
         $this->user_deads                = (!empty($data['user_deads'])) ? $data['user_deads'] : 0;
         $this->user_level                = (!empty($data['user_level'])) ? $data['user_level'] : null;
         $this->user_spirit               = (!empty($data['user_spirit'])) ? $data['user_spirit'] : 0;
         $this->user_intelligence         = (!empty($data['user_intelligence'])) ? $data['user_intelligence'] : null;
         $this->user_strength             = (!empty($data['user_strength'])) ? $data['user_strength'] : null;
         $this->user_charisma             = (!empty($data['user_charisma'])) ? $data['user_charisma'] : null;
         $this->user_tolerance            = (!empty($data['user_tolerance'])) ? $data['user_tolerance'] : null;
         $this->user_cash                 = (!empty($data['user_cash'])) ? $data['user_cash'] : 0;
         $this->user_credits              = (!empty($data['user_credits'])) ? $data['user_credits'] : 0;
         $this->user_points_inventory     = (!empty($data['user_points_inventory'])) ? $data['user_points_inventory'] : 0;
         $this->user_points_gang_donation = (!empty($data['user_points_gang_donation'])) ? $data['user_points_gang_donation'] : 0;
         $this->user_business_day  = (!empty($data['user_business_day'])) ? $data['user_business_day'] : 0; 
    }
}
