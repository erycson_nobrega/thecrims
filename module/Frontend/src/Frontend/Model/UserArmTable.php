<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UserArmTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function getEquippedUserArms($user_id) {
        return $this->tableGateway->select(['user_id' => $user_id, 'uarm_equipped' => 1]);
    }

    public function getUserArm($user_id, $arm_id) {
        $rowset = $this->tableGateway->select(['user_id' => $user_id, 'arm_id' => $arm_id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$user_id} - {$arm_id}");
        }
        return $row;
    }

    public function getUserArmByID($uarm_id) {
        $select = $this->tableGateway->select(function(Select $select) use ($uarm_id) {
            $select->join('arm', 'arm.arm_id=user_arm.arm_id');
            $select->where(['uarm_id' => $uarm_id]);
        });
        $rowset = $select->getDataSource();
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function getUserArmsBySection($user_id, $section) {
        $select = $this->tableGateway->select(function(Select $select) use ($user_id, $section) {
            $select->join('arm', 'arm.arm_id=user_arm.arm_id');
            $select->where(['user_id' => $user_id, 'arm_section' => $section]);
        });
        
        return $select->getDataSource();
    }

    public function saveUserArm(UserArm $user) {
        $data = array(
            'uarm_id'       => $user->uarm_id,
            'user_id'       => $user->user_id,
            'arm_id'        => $user->arm_id,
            'uarm_equipped' => $user->uarm_equipped,
        );

        $id = (int) $user->uarm_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getArm($id)) {
                $this->tableGateway->update($data, ['uarm_id' => $id]);
            } else {
                throw new \Exception('Arm id does not exist');
            }
        }
    }

    public function deleteArm($id) {
        $this->tableGateway->delete(['uarm_id' => $id]);
    }

}
