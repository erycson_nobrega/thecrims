<?php

namespace Frontend\Model;

class Drug {
    public $drug_id;
    public $drug_price_buy;
    public $drug_price_sell;
    public $drug_stock;
    
    public function exchangeArray($data)
    {
         $this->drug_id         = (!empty($data['drug_id'])) ? $data['drug_id'] : null;
         $this->drug_price_buy  = (!empty($data['drug_price_buy'])) ? $data['drug_price_buy'] : 0.0;
         $this->drug_price_sell = (!empty($data['drug_price_sell'])) ? $data['drug_price_sell'] : 0.0;
         $this->drug_stock      = (!empty($data['drug_stock'])) ? $data['drug_stock'] : 0;
    }
}
