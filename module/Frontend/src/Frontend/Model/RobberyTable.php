<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class RobberyTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        $select = $this->tableGateway->select(function(Select $select) {
            $select->order('robbery_difficult ASC');
        });
        return $select->getDataSource();
    }
      
    public function getRobbery($id) {
        $rowset = $this->tableGateway->select(['robbery_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }
}
