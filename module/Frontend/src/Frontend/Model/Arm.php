<?php

namespace Frontend\Model;

class Arm {
    const SECTION_WEAPON = 'weapons';
    const SECTION_ARMOR = 'armors';
    
    const TYPE_SUM = 'sum';
    const TYPE_PERCENTAGE = 'percentage';
    
    public $arm_id;
    public $arm_section;
    public $arm_type;
    public $arm_strength;
    public $arm_tolerance;
    public $arm_cash;
    public $arm_credits;
    public $arm_image;
    public $arm_required_level;
    
    public function exchangeArray($data)
    {
         $this->arm_id             = (!empty($data['arm_id'])) ? $data['arm_id'] : null;
         $this->arm_section        = (!empty($data['arm_section'])) ? $data['arm_section'] : null;
         $this->arm_type           = (!empty($data['arm_type'])) ? $data['arm_type'] : null;
         $this->arm_strength       = (!empty($data['arm_strength'])) ? $data['arm_strength'] : 0;
         $this->arm_tolerance      = (!empty($data['arm_tolerance'])) ? $data['arm_tolerance'] : 0;
         $this->arm_cash           = (!empty($data['arm_cash'])) ? $data['arm_cash'] : 0;
         $this->arm_credits        = (!empty($data['arm_credits'])) ? $data['arm_credits'] : 0;
         $this->arm_image          = (!empty($data['arm_image'])) ? $data['arm_image'] : null;
         $this->arm_required_level = (!empty($data['arm_required_level'])) ? $data['arm_required_level'] : 0;
    }
}
