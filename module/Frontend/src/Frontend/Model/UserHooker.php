<?php

namespace Frontend\Model;

class UserHooker {
    public $uhok_id;
    public $user_id;
    public $hok_id;
    public $amount;
    
    public function exchangeArray($data)
    {
         $this->uhok_id     = (!empty($data['uhok_id'])) ? $data['uhok_id'] : null;
         $this->user_id     = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->hok_id      = (!empty($data['hok_id'])) ? $data['hok_id'] : null;
         $this->uhok_amount = (!empty($data['uhok_amount'])) ? $data['uhok_amount'] : null;
    }
}
