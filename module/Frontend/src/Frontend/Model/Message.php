<?php

namespace Frontend\Model;

class Message {
    const TYPE_HOSPITAL = 'weapons';
    const TYPE_SQUARE = 'square';
    
    public $msg_id;
    public $user_id;
    public $msg_type;
    public $msg_day;
    public $msg_hour;
    public $msg_text;
    
    public function exchangeArray($data)
    {
         $this->msg_id   = (!empty($data['msg_id'])) ? $data['msg_id'] : null;
         $this->user_id  = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->msg_type = (!empty($data['msg_type'])) ? $data['msg_type'] : '';
         $this->msg_day  = (!empty($data['msg_day'])) ? $data['msg_day'] : 0;
         $this->msg_hour = (!empty($data['msg_hour'])) ? $data['msg_hour'] : 0;
         $this->msg_text = (!empty($data['msg_text'])) ? $data['msg_text'] : '';
    }
}
