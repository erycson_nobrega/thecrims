<?php

namespace Frontend\Model;

class Robbery {
    public $rob_id;
    public $rob_difficult;
    public $rob_energy;
    public $rob_spirit;
    public $rob_type;
    public $rob_min_reward;
    public $rob_max_reward;

    
    public function exchangeArray($data)
    {
         $this->rob_id         = (!empty($data['rob_id'])) ? $data['rob_id'] : null;
         $this->rob_difficult  = (!empty($data['rob_difficult'])) ? $data['rob_difficult'] : 0;
         $this->rob_energy     = (!empty($data['rob_energy'])) ? $data['rob_energy'] : 0;
         $this->rob_spirit     = (!empty($data['rob_spirit'])) ? $data['rob_spirit'] : 0;
         $this->rob_type       = (!empty($data['rob_type'])) ? $data['rob_type'] : 0;
         $this->rob_min_reward = (!empty($data['rob_min_reward'])) ? $data['rob_min_reward'] : 0.0;
         $this->rob_max_reward = (!empty($data['rob_max_reward'])) ? $data['rob_max_reward'] : 0.0;
    }
}
