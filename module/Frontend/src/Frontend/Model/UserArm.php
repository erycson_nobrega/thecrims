<?php

namespace Frontend\Model;

class UserArm {
    public $uarm_id;
    public $user_id;
    public $arm_id;
    public $uarm_equipped;
    
    public function exchangeArray($data)
    {
         $this->uarm_id       = (!empty($data['uarm_id'])) ? $data['uarm_id'] : null;
         $this->user_id       = (!empty($data['user_id'])) ? $data['user_id'] : null;
         $this->arm_id        = (!empty($data['arm_id'])) ? $data['arm_id'] : null;
         $this->uarm_equipped = (!empty($data['uarm_equipped'])) ? $data['uarm_equipped'] : 0;
    }
}
