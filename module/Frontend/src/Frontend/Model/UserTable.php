<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;

class UserTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getUser($id) {
        $rowset = $this->tableGateway->select(array('user_id' => (int) $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function saveUser(User $user) {
        $data = array(
            'user_username'             => $user->user_username,
            'user_password'             => $user->user_password,
            'user_email'                => $user->user_email,
            'user_info_accepted'        => $user->user_info_accepted,
            'user_country'              => $user->user_country,
            'user_date_register'        => $user->user_date_register,
            'user_date_last_login'      => $user->user_date_last_login,
            'user_date_of_birth'        => $user->user_date_of_birth,
            'user_profile_visitores'    => $user->user_profile_visitores,
            'user_time_online'          => $user->user_time_online,
            'user_times_banned'         => $user->user_times_banned,
            'user_active'               => $user->user_active,
            'user_profession'           => $user->user_profession,
            'user_stamina'              => $user->user_stamina,
            'user_addiction'            => $user->user_addiction,
            'user_respect'              => $user->user_respect,
            'user_genere'               => $user->user_genere,
            'user_kills'                => $user->user_kills,
            'user_deads'                => $user->user_deads,
            'user_level'                => $user->user_level,
            'user_spirit'               => $user->user_spirit,
            'user_intelligence'         => $user->user_intelligence,
            'user_strength'             => $user->user_strength,
            'user_charisma'             => $user->user_charisma,
            'user_tolerance'            => $user->user_tolerance,
            'user_cash'                 => $user->user_cash,
            'user_credits'              => $user->user_credits,
            'user_points_inventory'     => $user->user_points_inventory,
            'user_points_gang_donation' => $user->user_points_gang_donation,
            'user_business_day' => $user->user_business_day,
        );

        $id = (int) $user->user_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function deleteUser($id) {
        $this->tableGateway->delete(array('user_id' => (int) $id));
    }

}
