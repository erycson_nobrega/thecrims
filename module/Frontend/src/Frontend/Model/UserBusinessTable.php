<?php

namespace Frontend\Model;

use Zend\Db\TableGateway\TableGateway;

class UserBusinessTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function getUserBusinesses($user_id) {
        $rowset = $this->tableGateway->select(['user_id' => $user_id]);
        if (!$rowset) {
            throw new \Exception("Could not find rows for {$user_id}");
        }
        return $rowset;
    }

    public function getUserBusinessById($id) {
        $rowset = $this->tableGateway->select(['ubus_id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row {$id}");
        }
        return $row;
    }

    public function getUserBusinessByType($user_id, $type) {
        $select = $this->tableGateway->getSql()
            ->select()
            ->join('business', 'business.bus_id=user_business.bus_id')
            ->where(['user_id' => $user_id, 'bus_type' => $type]);
        
        $rowset = $this->tableGateway->selectWith($select);
        if (!$rowset) {
            throw new \Exception("Could not find rows for {$user_id}");
        }
        return $rowset;
    }

    public function saveBusiness(Business $business) {
        $data = array(
            'ubus_id' => $business->id,
            'user_id' => $business->user_id,
            'bus_id' => $business->business_id,
            'ubus_amount' => $business->amount,
        );

        $id = (int) $business->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUserBusinessById($id)) {
                $this->tableGateway->update($data, ['ubus_id' => $id]);
            } else {
                throw new \Exception('User Business id does not exist');
            }
        }
    }

    public function deleteUserBusinessById($id) {
        $this->tableGateway->delete(['ubus_id' => $id]);
    }

}
