<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return [
    
    'pusher' => [
        'host' => 'http://localhost:8080',
        'key'  => 'thecrims-pusher-key'
    ],
    'site' => [
        'secret-key' => 'thecrims-secret-key',
    ]
];
