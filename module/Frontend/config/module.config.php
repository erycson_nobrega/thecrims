<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'index' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'register' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/register',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Register',
                        'action'     => 'index',
                    ),
                ),
            ),
            'profile' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/profile',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Profile',
                        'action'     => 'index',
                    ),
                ),
            ),
            'reports' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/reports',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Reports',
                        'action'     => 'index',
                    ),
                ),
            ),
            'vip' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/vip',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Vip',
                        'action'     => 'index',
                    ),
                ),
            ),
            'rules' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/rules',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Rules',
                        'action'     => 'index',
                    ),
                ),
            ),
            'stuff' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/stuff',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Stuff',
                        'action'     => 'index',
                    ),
                ),
            ),
            'help' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/help',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Help',
                        'action'     => 'index',
                    ),
                ),
            ),
            'forum' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/forum',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Forum',
                        'action'     => 'index',
                    ),
                ),
            ),
            'winners' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/winners',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Winners',
                        'action'     => 'index',
                    ),
                ),
            ),
            'assault' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/assault',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Assault',
                        'action'     => 'index',
                    ),
                ),
            ),
            'robbery' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/robbery[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Robbery',
                        'action'     => 'index',
                    ),
                ),
            ),
            'hookers' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/hookers',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Hookers',
                        'action'     => 'index',
                    ),
                ),
            ),
            'armsdealer' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/armsdealer[/:section]',
                    'constraints' => array(
                        'section' => '[a-z]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Armsdealer',
                        'action'     => 'index',
                    ),
                ),
            ),
            'drugdealer' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/drugdealer[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Drugdealer',
                        'action'     => 'index',
                    ),
                ),
            ),
            'buildings' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/buildings',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Buildings',
                        'action'     => 'index',
                    ),
                ),
            ),
            'nightlife' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/nightlife',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Nightlife',
                        'action'     => 'index',
                    ),
                ),
            ),
            'businesses' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/businesses',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Businesses',
                        'action'     => 'index',
                    ),
                ),
            ),
            'hospital' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/hospital',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Hospital',
                        'action'     => 'index',
                    ),
                ),
            ),
            'prison' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/prison',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Prison',
                        'action'     => 'index',
                    ),
                ),
            ),
            'bank' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/bank',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Bank',
                        'action'     => 'index',
                    ),
                ),
            ),
            'casino' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/casino',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Casino',
                        'action'     => 'index',
                    ),
                ),
            ),
            'alley' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/alley',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Alley',
                        'action'     => 'index',
                    ),
                ),
            ),
            'market' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/market',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Market',
                        'action'     => 'index',
                    ),
                ),
            ),
            'thesquare' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/thesquare',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Thesquare',
                        'action'     => 'index',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/login',
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Login',
                        'action'     => 'index',
                    ),
                ),
            ),
            'stats' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/stats[/:action]',
                    'constraints' => array(
                        'action' => '[a-z]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Stats',
                        'action'     => 'index',
                    ),
                ),
            ),
            'start' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/start[/:action]',
                    'constraints' => array(
                        'action' => '[a-z]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Frontend\Controller\Start',
                        'action'     => 'index',
                    ),
                ),
            ),
            'frontend' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Frontend\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action' => 'index',
                                '__NAMESPACE__' => 'Frontend\Controller'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    
    'translator' => [
        'locale' => 'gb',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
    
    'service_manager' => [
        'factories' => [
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ],
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
    ],
    
    'controllers' => array(
        'invokables' => array(
            'Frontend\Controller\Index'      => 'Frontend\Controller\IndexController',
            'Frontend\Controller\Register'   => 'Frontend\Controller\RegisterController',
            'Frontend\Controller\Stats'      => 'Frontend\Controller\StatsController',
            'Frontend\Controller\Start'      => 'Frontend\Controller\StartController',
            'Frontend\Controller\Winners'    => 'Frontend\Controller\WinnersController',
            'Frontend\Controller\Login'      => 'Frontend\Controller\LoginController',
            'Frontend\Controller\Assault'    => 'Frontend\Controller\AssaultController',
            'Frontend\Controller\Robbery'    => 'Frontend\Controller\RobberyController',
            'Frontend\Controller\Hookers'    => 'Frontend\Controller\HookersController',
            'Frontend\Controller\Armsdealer' => 'Frontend\Controller\ArmsdealerController',
            'Frontend\Controller\Drugdealer' => 'Frontend\Controller\DrugdealerController',
            'Frontend\Controller\Buildings'  => 'Frontend\Controller\BuildingsController',
            'Frontend\Controller\Nightlife'  => 'Frontend\Controller\NightlifeController',
            'Frontend\Controller\Businesses' => 'Frontend\Controller\BusinessesController',
            'Frontend\Controller\Hospital'   => 'Frontend\Controller\HospitalController',
            'Frontend\Controller\Prison'     => 'Frontend\Controller\PrisonController',
            'Frontend\Controller\Bank'       => 'Frontend\Controller\BankController',
            'Frontend\Controller\Casino'     => 'Frontend\Controller\CasinoController',
            'Frontend\Controller\Alley'      => 'Frontend\Controller\AlleyController',
            'Frontend\Controller\Market'     => 'Frontend\Controller\MarketController',
            'Frontend\Controller\Thesquare'  => 'Frontend\Controller\ThesquareController',
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'base_path'                => 'http://thecrims.local',
        'template_map' => array(
            'layout/layout'        => __DIR__ . '/../view/layout/layout.phtml',
            'frontend/index/index' => __DIR__ . '/../view/frontend/index/index.phtml',
            'error/404'            => __DIR__ . '/../view/error/404.phtml',
            'error/index'          => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    
    'log' => array(
        'Log\App' => array(
            'writers' => array(
                array(
                    'name' => 'stream',
                    'priority' => 1000,
                    'options' => array(
                        'stream' => 'data/logs/app.log',
                    ),
                ),
            ),
        ),
    ),
    
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);